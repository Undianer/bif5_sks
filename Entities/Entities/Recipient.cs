﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opitz_Unterbrunner_SKS.Entities
{
    class Recipient
    {
        private string name;
        private Address address;
        private Coords coords;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
      
        internal Address Address
        {
            get { return address; }
            set { address = value; }
        }
    
        internal Coords Coords
        {
            get { return coords; }
            set { coords = value; }
        }

    }
}

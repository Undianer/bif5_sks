﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EntityRepo.Entities
{
	public class Region
	{
        [Key]
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
		public virtual Address Address { get; set; }
		public virtual Coords Coords { get; set; }
        public List<Package> PackageList { get; set; }

		public Region()
		{
			this.PackageList = new List<Package>();
		}

		public Region(string name)
		{
			this.Name = name;
			this.PackageList = new List<Package>();
		}

		public Region(string key, string name, string location, string postcode, string street)
		{
            this.Key = key;
			this.Name = name;
			this.Address = new Address(location, postcode, street);
			this.PackageList = new List<Package>();
			this.Coords = new Coords();
		}

		public void AddPackage(Package package)
		{
			this.PackageList.Add(package);
		}
	}
}

﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EntityRepo.Entities
{
    [DataContract]
    public class Coords
    {
        [Key]
        [DataMember(Name = "Id", EmitDefaultValue = false)]
        public int Id { get; set; }
        [DataMember(Name = "Lontitude", EmitDefaultValue = false)]
        public double Lon { get; set; }
        [DataMember(Name = "Latitude", EmitDefaultValue = false)]
        public double Lat { get; set; }

        public Coords() { }

        public Coords(double Lon, double Lat)
        {
            this.Lon = Lon;
            this.Lat = Lat;
        }
    }
}

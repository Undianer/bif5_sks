﻿using System.ComponentModel.DataAnnotations;

namespace EntityRepo.Entities
{
    public class DeliveryPerson
    {
        [Key]
        public int Id { get; set; }
        public int Svn { get; set; }
        public string Name { get; set; }

        public DeliveryPerson()
        {

        }

		public DeliveryPerson(int svn, string name) 
		{
			this.Svn = svn;
			this.Name = name;
		}

		
    }
}

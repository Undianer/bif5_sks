﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;

namespace EntityRepo.Entities
{
    public class Package
    {
		//[ForeignKey("Address")]
		//public int AddressId { get; set; }
		//[ForeignKey("Coords")]
		//public int CoordsId { get; set; }
		//[ForeignKey("Region")]
		//public int RegionId { get; set; }
        [Key]
        public int Id { get; set; }
        public string Recipient { get; set; }
		public bool Delivered { get; set; }
        public virtual Address Address { get; set; }
        public virtual Coords Coords { get; set; }
        public virtual Region Region { get; set; }

		public Package()
		{

		}

		public Package(string recipient, Address address)
		{
			this.Address = address;
			this.Recipient = recipient;
			this.Delivered = false;
		}

		public Package(string recipient, Address address, Coords coords)
		{
			this.Recipient = recipient;
			this.Address = address;
			this.Coords = coords;
			this.Delivered = false;
		}

		public Package(string recipient, string location, string postcode, string street)
		{
			this.Recipient = recipient;
			this.Address = new Address(location, postcode, street);
			this.Delivered = false;
		}

		public Package(string recipient, string location, string postcode, string street, double Lon, double Lat, string regionname)
		{
			this.Recipient = recipient;
			this.Address = new Address(location, postcode, street);
			this.Coords = new Coords(Lon, Lat);
			this.Region = new Region(regionname);
			this.Region.AddPackage(this);
			this.Delivered = false;
		}

    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EntityRepo.Entities
{
    [DataContract]
    public class Address
    {
        [Key]
        [DataMember(Name = "Id", EmitDefaultValue = false)]
        public int Id { get; set; }
        [DataMember(Name = "Location", EmitDefaultValue = false)]
        public string Location { get; set; }
        [DataMember(Name = "Postcode", EmitDefaultValue = false)]
        public string Postcode { get; set; }
        [DataMember(Name = "Street", EmitDefaultValue = false)]
        public string Street { get; set; }

        public Address() { }

        public Address(string location, string postcode, string street)
        {
            this.Location = location;
            this.Postcode = postcode;
            this.Street = street;
        }


        public bool Equals(Address compare)
        {
            if (this.Location.Equals(compare.Location) && this.Postcode.Equals(compare.Postcode) && this.Street.Equals(compare.Street))
            {
                return true;
            }
            return false;
        }

    }
}

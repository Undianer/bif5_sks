﻿using System.Data.Entity;
using EntityRepo.Entities;

namespace EntityRepo
{
	public class EntityContext : DbContext
	{
		public EntityContext() : base("DB") { }

		public DbSet<Address> Addresses { get; set; }
		public DbSet<Coords> Coordses { get; set; }
		public DbSet<DeliveryPerson> DeliveryPersons { get; set; }
		public DbSet<Package> Packages { get; set; }
		public DbSet<Region> Regions { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Presentation
{
	public interface IPackageLogic
	{
		IEnumerable<Package> GetAll();
		IEnumerable<Package> GetDeliveredByRegionKey(string regionKey, bool delivered);
		IEnumerable<Package> DeliverPackagesFromRegion(string regionKey);
	}
}

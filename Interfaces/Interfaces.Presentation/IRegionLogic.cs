﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Presentation
{
	public interface IRegionLogic
	{
		IEnumerable<Region> GetAll();
		Region GetById(int id);
		void Delete(Region region);
		void Update(Region region);
		bool ImportRegions(XmlDocument doc);
	}
}

﻿using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Services
{
	public interface IPackageShipper
	{
		void AddPackage(Package package);
	}
}

﻿using System.Collections.Generic;
using DALInterfaces.Interfaces.Agents;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Services
{
	public interface IRegionImporter
	{
		IEnumerable<Region> GetRegion(string id);
		bool ImportRegion(List<Region> regionList, IGeodataServiceAgent geodataService);
	}
}

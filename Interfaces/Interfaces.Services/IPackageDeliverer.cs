﻿using System.Collections.Generic;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Services
{
	public interface IPackageDeliverer
	{
		IEnumerable<Package> GetPackagesFromRegion(string regionKey);
	}
}

﻿using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Services
{
    public interface IPackageSorter
    {
        Package ArrangePackage(Package package);
    }
}

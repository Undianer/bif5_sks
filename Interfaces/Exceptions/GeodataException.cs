﻿using System;

namespace DALInterfaces.Exceptions
{
    public class GeodataException : Exception
    {
        public GeodataException() { }

        public GeodataException(string message)
            : base(message)
        {

        }

        public GeodataException(string message, Exception inner)
        :base(message, inner)
        { 
        
        }
    }
}

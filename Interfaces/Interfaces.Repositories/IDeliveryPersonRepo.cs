﻿using System.Collections.Generic;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Repositories
{
	public interface IDeliveryPersonRepo : IRepository<DeliveryPerson>
	{
		IEnumerable<DeliveryPerson> GetByName(string name);
		DeliveryPerson GetBySvn(int svn);
	}
}

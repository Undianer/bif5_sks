﻿using System.Collections.Generic;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Repositories
{
	public interface IPackageRepo : IRepository<Package>
	{
		IEnumerable<Package> GetByLocation(string location);
		IEnumerable<Package> GetByPostcode(string postcode);
		IEnumerable<Package> GetByStreet(string location, string street);
		IEnumerable<Package> GetByStreetAndPostcode(string postcode, string street);
		IEnumerable<Package> GetByAddress(Address address);
		IEnumerable<Package> GetByRegionKey(string key);
		IEnumerable<Package> DeliverPackagesFromRegion(string key);
		IEnumerable<Package> GetDeliveredPackagesByRegionKey(string key, bool state);
	}
}

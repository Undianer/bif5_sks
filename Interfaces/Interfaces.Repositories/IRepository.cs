﻿using System.Collections.Generic;

namespace DALInterfaces.Interfaces.Repositories
{
	public interface IRepository<T>
	{
		void Add(T obj);
		void Update(T old, T obj);
		void Delete(T obj);
		IEnumerable<T> GetAll();
	}
}

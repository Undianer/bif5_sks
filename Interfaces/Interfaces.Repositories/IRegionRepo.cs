﻿using System.Collections.Generic;
using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Repositories
{
    public interface IRegionRepo : IRepository<Region>
    {
        IEnumerable<Region> GetByKey(string key);
        IEnumerable<Region> GetByName(string name);
	    Region GetById(int id);
    }
}

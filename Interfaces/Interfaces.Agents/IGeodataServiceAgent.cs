﻿using EntityRepo.Entities;

namespace DALInterfaces.Interfaces.Agents
{
	public interface IGeodataServiceAgent
	{
		double[] EncodeCoordinates(Address address);
	}
}

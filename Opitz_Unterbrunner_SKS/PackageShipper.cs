﻿using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS
{
	public class PackageShipper : IPackageShipper
	{
		private IPackageRepo PackageRepo { get; set; }
        private IPackageSorter PackageSorter { get; set; }

		private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public PackageShipper(IPackageRepo repo, IPackageSorter pss)
		{
			this.PackageRepo = repo;
            this.PackageSorter = pss;
		}

		public void AddPackage(Package package)
		{
            package = PackageSorter.ArrangePackage(package);
			try
			{
				PackageRepo.Add(package);
			}
			catch(RepositoryException ex)
			{
				log.Error("Error occurred in PackageShipper.AddPackage", ex);
				throw ex;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS.Mocks
{
    public class MockPackageSorter : IPackageSorter
    {
        public IGeodataServiceAgent geodataService { get; set; }
        public List<Region> regionList { get; set; }
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MockPackageSorter(IGeodataServiceAgent geodataService)
        {
            this.geodataService = geodataService;
            regionList = new List<Region>();
            regionList.Add(new Region("Key1", "Region1", "Wien", "1200", "Höchstädtplatz 5"));
            regionList.Add(new Region("Key2", "Region2", "Wien", "1120", "Längenfeldgasse 12"));
            regionList.Add(new Region("Key3", "Region3", "Wien", "1090", "Alserbachstraße 10"));
            regionList.Add(new Region("Key4", "Region4", "Wien", "1010", "Rotenturmstraße 11"));

        }
        public Package ArrangePackage(Package package)
        {
            package.Coords = GetCoords(package.Address);
            package.Region = CalculateNearestRegion(package.Coords);
            log.Info("Nearest Region von " + package.Id + " = " + package.Region.Key);
            return package;
        }

        private Coords GetCoords(Address address)
        {
            try
            {
                return new GeodataMapper(geodataService).MapLatLongToCoords(address);
            }
            catch (GeodataException ex)
            {
                log.Error("Exception occurred in MockPackageSorter.GetPackageCoords when fetching Geodata", ex);
                return null;
            }
        }

        private Package MapPackageToRegionByCoords(Package package)
        {

            return package;
        }

        private Region CalculateNearestRegion(Coords coords)
        {
            // DbRegionRepo repo = new DbRegionRepo();
            log.Info("Calculating nearest Region in Mock...");
            double lat = 0;
            double lon = 0;
            double nearestVector = 0;
            Region temp = null;

            foreach (Region r in regionList)
            {
                lat = Math.Pow(r.Coords.Lat - coords.Lat, 2);
                lon = Math.Pow(r.Coords.Lon - coords.Lon, 2);

                if (nearestVector == 0)
                {
                    nearestVector = lat + lon;
                    temp = r;
                }
                if (nearestVector > (lat + lon))
                {
                    temp = r;
                }
            }
            return temp;
        }

    }

}

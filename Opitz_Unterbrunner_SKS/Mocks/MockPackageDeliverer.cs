﻿using System.Collections.Generic;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;

namespace Opitz_Unterbrunner_SKS.Mocks
{
	public class MockPackageDeliverer : IPackageDeliverer
	{
		public IEnumerable<Package> GetPackagesFromRegion(string regionKey)
		{
			List<Package> packageList = new List<Package>();
			packageList.Add(new Package("Max Knorr", "Wien", "A-1200", "Hochstädtplatz 4", 23.00, 23.00, "Wien Nordregion"));
			packageList.Add(new Package("Andreas Unterbrunner", "Wien", "A-1200", "Längenfeldgasse 12", 11.00, 11.23, "Wien Nordregion"));
			packageList.Add(new Package("Stefan Opitz", "Wien", "A-1200", "Hauptstraße 75", 1337.00, 42.00, "Wien Nordregion"));

			return packageList;
		}
	}
}

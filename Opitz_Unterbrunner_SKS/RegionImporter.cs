﻿using System.Collections.Generic;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS
{
    public class RegionImporter : IRegionImporter
    {
		IRegionRepo RegionRepo { get; set; }
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RegionImporter(IRegionRepo repo)
        {
			RegionRepo = repo;
        }

        public IEnumerable<Region> GetRegion(string id)
        {
            try
            {
                return RegionRepo.GetByKey(id);
            }
            catch (RepositoryException ex)
            {
                log.Error("Error occured in XmlImportServices.RegionImporter.GetRegion returning null", ex);
                return null;
            }
        }

        public bool ImportRegion(List<Region> regionList, IGeodataServiceAgent geodataService)
        {
            GeodataMapper mapper = new GeodataMapper(geodataService);
            List<Region> tempList = new List<Region>();
            try
            {
                foreach (Region r in regionList)
                {
                    r.Coords = mapper.MapLatLongToCoords(r.Address);
                    tempList.Add(r);

                }
                if (tempList.Count == regionList.Count)
                {
                    foreach (Region r in tempList)
                    {
                        RegionRepo.Add(r);
                    }
                }
                return true;
            }
            catch (RepositoryException ex)
            {
                log.Error("Error occured in XmlImportServices.RegionImporter.ImportRegions", ex);
                throw;
            }
            catch (GeodataException ex)
            {
                log.Error("Error occured in XmlImportServices.RegionImporter.ImportRegions", ex);
                throw;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Presentation;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS.PresentationLogic
{
	public class PackageLogic : IPackageLogic
	{
		private IPackageRepo packageRepo;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		public PackageLogic(IPackageRepo repo)
		{
			packageRepo = repo;
		}

		public IEnumerable<Package> GetAll()
		{
			try
			{
                log.Info("BusinessLogic.PackageLogic.GetAll(), Lade alle Packages");
				return packageRepo.GetAll();
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.PackageLogic.GetAll(), Fehler beim Laden der Packages", ex);
				throw ex;
			}
		}

		public IEnumerable<Package> GetDeliveredByRegionKey(string regionKey, bool delivered)
		{
			try
			{
				return packageRepo.GetDeliveredPackagesByRegionKey(regionKey, delivered);
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.PackageLogic.GetDeliveredByRegionKey(string regionKey, bool delivered), Fehler beim Laden der Packages", ex);
				throw ex;
			}
		}

		public IEnumerable<Package> DeliverPackagesFromRegion(string regionKey)
		{
			try
			{
				return packageRepo.DeliverPackagesFromRegion(regionKey);
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.PackageLogic.DeliverPackagesFromRegion(string regionKey), Fehler beim Liefern der Packages", ex);
				throw ex;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Presentation;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS.PresentationLogic
{
	public class RegionLogic : IRegionLogic
	{
		private IRegionRepo regionRepo;
		private IRegionImporter importer;
		private IGeodataServiceAgent geodataService;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		public RegionLogic(IRegionRepo repo, IRegionImporter importer, IGeodataServiceAgent geodata)
		{

			this.regionRepo = repo;
			this.importer = importer;
			this.geodataService = geodata;
        
		}

		public IEnumerable<EntityRepo.Entities.Region> GetAll()
		{
			try
			{
                log.Info("RegionLogic.GetAll(), Hole alle Regions");
				return regionRepo.GetAll();
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.RegionLogic.GetAll(), Fehler beim Laden der Regions", ex);
				throw ex;
			}
		}

		public EntityRepo.Entities.Region GetById(int id)
		{
			try
			{
                log.Info("RegionLogic.GetById(), Hole Region");
				return regionRepo.GetById(id);
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.RegionLogic.GetById(int id), Fehler beim Laden der Region", ex);
				throw ex;
			}
		}

		public void Delete(EntityRepo.Entities.Region region)
		{
			try
			{
                log.Info("RegionLogic.Delete(), Lösche Region");
				regionRepo.Delete(region);
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.RegionLogic.Delete(EntityRepo.Entities.Region region), Fehler beim Löschen der Region", ex);
				throw ex;
			}
		}

		public void Update(EntityRepo.Entities.Region region)
		{
			try
			{
                log.Info("RegionLogic.Update(), Update Regions");
				regionRepo.Update(null, region);
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.RegionLogic.Update(EntityRepo.Entities.Region region), Fehler beim Update der Region", ex);
				throw ex;
			}
		}


		public bool ImportRegions(XmlDocument doc)
		{
			List<Region> regions = new List<Region>();

			try
			{
                log.Info("RegionLogic.ImportRegions, Lade XML-File");
				XmlNodeList nodes = doc.DocumentElement.ChildNodes;

				foreach (XmlNode node in nodes)
				{
					regions.Add(MapXmlToRegion(node));
				}
			}
			catch (NullReferenceException ex)
			{
                log.Error("BusinessLogic.RegionLogic.ImportRegions(XmlDocument doc), Fehler beim XMLImport der Regions", ex);
			}

			if (regions.Any())
			{
				return importer.ImportRegion(regions, geodataService);
			}
			return false;
		}

		public Region MapXmlToRegion(XmlNode node)
		{
			try
			{
				return new Region(
					node.Attributes["Key"].InnerText,
					node.SelectSingleNode("DisplayName").InnerText,
					node.SelectSingleNode("Address/City").InnerText,
					node.SelectSingleNode("Address/PostalCode").InnerText,
					node.SelectSingleNode("Address/Street").InnerText
				);
			}
			catch (NullReferenceException ex)
			{
                log.Error("BusinessLogic.RegionLogic.MapXmlToRegion(XmlNode node), Fehler beim Mappen der Regions", ex);
				throw ex;
			}
		}
	}
}

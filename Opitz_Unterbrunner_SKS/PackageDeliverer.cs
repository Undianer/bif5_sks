﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net.Repository.Hierarchy;
using log4net;

namespace Opitz_Unterbrunner_SKS
{
	public class PackageDeliverer : IPackageDeliverer
	{
		private IPackageRepo PackageRepo { get; set; }
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		public PackageDeliverer(IPackageRepo repo)
		{
			PackageRepo = repo;
		}

		public IEnumerable<Package> GetPackagesFromRegion(string regionKey)
		{
			try
			{
				return PackageRepo.DeliverPackagesFromRegion(regionKey);
			}
			catch (RepositoryException ex)
			{
                log.Error("BusinessLogic.PackageDeliverer.GetPackagesFromRegion(string regionKey), Fehler beim Laden der Regions", ex);
				throw ex;
			}
		}
	}
}

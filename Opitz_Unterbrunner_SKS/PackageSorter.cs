﻿using System;
using System.Collections.Generic;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS
{
    public class PackageSorter : IPackageSorter
    {
        public IGeodataServiceAgent geodataService { get; set; }
        public IRegionRepo regionRepo { get; set; }

        public PackageSorter(IGeodataServiceAgent geodataService, IRegionRepo regionRepo)
        {
            this.geodataService = geodataService;
            this.regionRepo = regionRepo;
        }

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Package ArrangePackage(Package package)
        {
            try
            {
                package.Coords = GetCoords(package.Address);
            }
            catch (RepositoryException ex)
            {
                log.Error("Error occured in PackageSorter.ArrangePackage.GetCoords()", ex);
            }
            try
            {
                package.Region = CalculateNearestRegion(package.Coords);
            }
            catch (RepositoryException ex)
            {
                log.Error("Error occured in PackageSorter.ArrangePackage.CalculateNearestRegion()", ex);
            }
            return package;
        }

        private Coords GetCoords(Address address)
        {
            try
            {
                return new GeodataMapper(geodataService).MapLatLongToCoords(address);
            }
            catch (GeodataException ex)
            {
                log.Error("Exception occurred in PackageSorter.GetPackageCoords when fetching Geodata", ex);
                return null;
            }
        }

        private Package MapPackageToRegionByCoords(Package package)
        {

            return package;
        }

        private Region CalculateNearestRegion(Coords coords)
        {
            // DbRegionRepo repo = new DbRegionRepo();
            log.Info("Calculating nearest Region...");
            List<Region> regionList = (List<Region>)regionRepo.GetAll();
            double lat = 0;
            double lon = 0;
            double nearestVector = 0;
            Region temp = null;

            foreach (Region r in regionList)
            {
                lat = Math.Pow(r.Coords.Lat - coords.Lat, 2);
                lon = Math.Pow(r.Coords.Lon - coords.Lon, 2);

                if (nearestVector == 0)
                {
                    nearestVector = lat + lon;
                    temp = r;
                }
                if (nearestVector > (lat + lon))
                {
                    temp = r;
                }
            }
            return temp;
        }

    }
}

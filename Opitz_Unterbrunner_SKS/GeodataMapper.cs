﻿using DALInterfaces.Interfaces.Agents;
using EntityRepo.Entities;
using log4net;

namespace Opitz_Unterbrunner_SKS
{
	public class GeodataMapper
	{
		private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public IGeodataServiceAgent GeodataServiceAgent { get; set; }

		public GeodataMapper(IGeodataServiceAgent geodataService)
		{
			GeodataServiceAgent = geodataService; 
		}

		public Coords MapLatLongToCoords(Address address)
		{
            double[] coordinates = new double[2];
            coordinates = GeodataServiceAgent.EncodeCoordinates(address);
			return new Coords(coordinates[0], coordinates[1]);
		}
	}
}

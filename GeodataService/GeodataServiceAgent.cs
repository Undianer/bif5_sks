﻿using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using EntityRepo.Entities;
using log4net;
using System;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace GeodataService
{
    public class GeodataServiceAgent : IGeodataServiceAgent
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //Für scheiß BING
        // private string key = "Aog295OPLfWGA57GaE9X3gVX_YeEgzXSaTdBCXB2H0BHRbofYYBHD4p2VeoH4R41";
        private bool logger = true;
        public double[] EncodeCoordinates(Address address)
        {
            if (logger)
            {
                log4net.Config.XmlConfigurator.Configure();
            }
            logger = false;
            log.Info("Searching Coordinates from Google");
            double[] result = null;
            string country = HttpUtility.UrlEncode("Österreich"); //oder "-"
            string postcode = HttpUtility.UrlEncode(address.Postcode);
            string location = HttpUtility.UrlEncode(address.Location);
            string street = HttpUtility.UrlEncode(address.Street);
            string connString = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}+{1},+{2},+{3}&sensor=false", postcode, street, location, country);
            //BING Schaß
            // string connString = string.Format("http://dev.virtualearth.net/REST/v1/Locations?countryRegion={0}&locality={1}&postalCode={2}&addressLine={3}&maxResults=1&key={4}", country, postcode, location, street, key);
            Uri geocodeRequest = new Uri(connString);

            result = MakeRequest(geocodeRequest);

            return result;
        }

        public static double[] MakeRequest(Uri requestUrl)
        {
            HttpWebRequest request;
            HttpWebResponse response = null;
            //StreamReader reader;
            //StringBuilder sbSource;
            double[] result = new double[2];

            request = WebRequest.Create(requestUrl) as HttpWebRequest;
            request.UserAgent = "dastan";
            request.KeepAlive = false;
            request.Timeout = 10 * 1000;

            try
            {
                response = request.GetResponse() as HttpWebResponse;

                XDocument xdoc = XDocument.Load(response.GetResponseStream());

                // the GeocodeResponse status, vielleicht für überprüfen
                string status = xdoc.Element("GeocodeResponse").Element("status").Value;

                XElement locationElement = xdoc.Element("GeocodeResponse").Element("result").Element("geometry").Element("location");
                result[0] = (double)locationElement.Element("lng");
                result[1] = (double)locationElement.Element("lat");


            }
            catch (WebException ex)
            {
                log.Error("GeodataServiceAgent.MakeRequest; Google Response Fehler!");
                throw new GeodataException("in GeodataServiceAgent.makeRequest", ex) ;
            }

            return result;
        }


    }

}



﻿using DALInterfaces.Interfaces.Agents;
using EntityRepo.Entities;
using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeodataService
{
    public class GeodataServiceMock : IGeodataServiceAgent
    {
        public double[] MockCoords;

        public GeodataServiceMock()
        {
            MockCoords = new double[2];
            MockCoords[0] = 40.5555555;
            MockCoords[1] = 20.5555555;
        }
        public double[] EncodeCoordinates(Address address)
        {

            MockCoords = MakeRequest();
            return MockCoords;

        }

        public double[] MakeRequest()
        {

            string xmlResponse = 
            "<GeocodeResponse><status>OK</status><result><type>street_address</type><formatted_address>Dresdner Straße 1, 1200 Wien, Österreich</formatted_address>" +
            "<address_component><long_name>1</long_name><short_name>1</short_name><type>street_number</type></address_component><address_component><long_name>Dresdner Straße</long_name><short_name>Dresdner Str.</short_name>" +
            "<type>route</type></address_component><address_component><long_name>Brigittenau</long_name><short_name>Brigittenau</short_name><type>sublocality</type><type>political</type></address_component>" +
            "<address_component><long_name>Wien</long_name><short_name>Wien</short_name><type>locality</type><type>political</type></address_component><address_component><long_name>Wien</long_name><short_name>Wien</short_name>" +
            "<type>administrative_area_level_1</type><type>political</type></address_component><address_component><long_name>Österreich</long_name><short_name>AT</short_name><type>country</type><type>political</type>" +
            "</address_component><address_component><long_name>1200</long_name><short_name>1200</short_name><type>postal_code</type></address_component><geometry>" +
            //------------------------------------------------------------------
            "<location><lat>40.5555555</lat><lng>20.5555555</lng></location>" + //Was wir abfragen
            //------------------------------------------------------------------
            "<location_type>RANGE_INTERPOLATED</location_type><viewport><southwest><lat>48.2401669</lat><lng>16.3734895</lng></southwest><northeast><lat>48.2428648</lat><lng>16.3761875</lng></northeast>" +
            "</viewport><bounds><southwest><lat>48.2415133</lat><lng>16.3748301</lng></southwest><northeast><lat>48.2415184</lat><lng>16.3748469</lng></northeast></bounds></geometry></result></GeocodeResponse>";
            double[] result = new double[2];
            XDocument xdoc = XDocument.Parse(xmlResponse);
            XElement locationElement = xdoc.Element("GeocodeResponse").Element("result").Element("geometry").Element("location");
            result[0] = (double)locationElement.Element("lat");
            result[1] = (double)locationElement.Element("lng");

            return result;
        }
    }
}

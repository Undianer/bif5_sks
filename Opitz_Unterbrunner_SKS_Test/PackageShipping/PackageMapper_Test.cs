﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShippingService.Mapper;
using Address = ShippingService.Address;
using Package = ShippingService.Package;

namespace Opitz_Unterbrunner_SKS_Test.PackageShipping
{
    [TestClass]
    public class PackageMapper_Test
    {
        public Package Dto { get; set; }
        
        public PackageMapper_Test()
        {
            Dto = new Package();
            Dto.Address = new Address();
            Dto.Address.City = "Vienna";
            Dto.Address.Country = "Austria";
            Dto.Address.PostalCode = "1200";
            Dto.Address.Street = "Höchststädtplatz 5";
        }

        [TestMethod]
        public void DtoToPackage_MapDtoToPackage_Success()
        {
            // Arrange
            PackageMapper pm = new PackageMapper();
            EntityRepo.Entities.Package result;

            // Act
            result = pm.MapDtoToPackage(Dto);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Address.Location, Dto.Address.City);

        }
    }
}

﻿using EntityRepo.Entities;
using GeodataService;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Opitz_Unterbrunner_SKS.Mocks;

namespace Opitz_Unterbrunner_SKS_Test.PackageSorter
{
    [TestClass]
    public class PackageSortingService_Test
    {
        private static readonly ILog log = LogManager.GetLogger(global::System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [TestMethod]
        public void ArrangePackage_Test_GetCoords()
        {
            log.Info("PackageSortingService_Test.ArrangePackage_Test started");
            //Arrange
            Package package = new Package("Anonymus", "Vienna", "1200", "Höchstädtplatz 5");
            MockPackageSorter pss = new MockPackageSorter(new GeodataServiceAgent());
            Package result = null;
            //Act
            result = pss.ArrangePackage(package);

            //Assert
            Assert.IsNotNull(result.Coords);
    
        }
        public void ArrangePackage_Test_CalculateNearestRegion()
        {
            log.Info("PackageSortingService_Test.ArrangePackage_Test started");
            //Arrange
            Package package = new Package("Anonymus", "Vienna", "1200", "Höchstädtplatz 5");
            MockPackageSorter pss = new MockPackageSorter(new GeodataServiceAgent());
            Package result = null;
            //Act
            result = pss.ArrangePackage(package);

            //Assert
            Assert.IsNotNull(result.Region);
        }
    }
}

﻿using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackageDeliveryService.Mapper;

namespace Opitz_Unterbrunner_SKS_Test.PackageDelivery
{
    [TestClass]
    public class PackageMapper_Test
    {
        Package package;
        Package[] packages;
        public PackageMapper_Test()
        {
            package = new Package("Anonymous", "Vienna", "1200", "Höchstädtplatz 5");
            packages = new Package[] 
            {
                package,
                new Package("Anonymous1", "Vienna1", "1201", "Höchstädtplatz 6"),
                new Package("Anonymous2", "Vienna2", "1202", "Höchstädtplatz 7")
            };
        }

        [TestMethod]
        public void PackageToDto_MapPackageToDto_Success()
        {
            //Arrange
            PackageDeliveryService.Package result;
            PackageMapper pm = new PackageMapper(); 

            //Act
            result = pm.MapPackageToDto(package);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Address.City, package.Address.Location);
        }

        [TestMethod]
        public void PackageArrayToDtoArray_MapPackageArrayToDtoArray_Success()
        { 
            //Arrange
            PackageDeliveryService.Package[] results;
            PackageMapper pm = new PackageMapper();

            //Act
            results = pm.MapPackageArrayToDtoArray(packages);

            //Assert
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Length == 3);
            Assert.AreEqual(results[0].Address.City, "Vienna");
        }
    }
}

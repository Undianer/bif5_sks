﻿using System.Collections.Generic;
using System.Linq;
using DAL.Mock;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Opitz_Unterbrunner_SKS_Test.MockRepositories
{
	[TestClass]
	public class InMemoryPackage_Test
	{
		private InMemoryPackage iMPackage;

		public InMemoryPackage_Test()
		{
			iMPackage = new InMemoryPackage();
		}

		[TestMethod]
		public void GetByLocation_ListOfPackages_Test()
		{
			string toSearch = "Wien";
			List<Package> expectedResult = new List<Package> 
						{ 
							new Package("Dawn Alolino", "Wien", "1200", "Dresdner Strasse 3"),
							new Package("Agnes Unterbrunner", "Wien", "1200", "Laengenfeldgasse 95")
						};
			List<Package> result = new List<Package>( iMPackage.GetByLocation(toSearch) );

			Assert.AreEqual(result[0].Address.Location, expectedResult[1].Address.Location);
		}

		[TestMethod]
		public void GetByPostcode_ListOfPackages_Test()
		{
			string toSearch = "1200";
			List<Package> expectedResult = new List<Package> 
						{ 
							new Package("Dawn Alolino", "Wien", "1200", "Dresdner Strasse 3"),
							new Package("Agnes Unterbrunner", "Wien", "1200", "Laengenfeldgasse 95")
						};
			List<Package> result = new List<Package>(iMPackage.GetByPostcode(toSearch));

			Assert.AreEqual(result[0].Address.Postcode, expectedResult[1].Address.Postcode);
		}

		[TestMethod]
		public void GetByStreetAndLocation_ListOfPackages_Test()
		{
			string toSearch = "Dresdner Strasse 3";
			List<Package> expectedResult = new List<Package> 
						{ 
							new Package("Dawn Alolino", "Wien", "1200", "Dresdner Strasse 3"),
						};
			List<Package> result = new List<Package>(iMPackage.GetByStreet("Wien", toSearch));

			Assert.AreEqual(result[0].Address.Location, expectedResult[0].Address.Location);
			Assert.IsFalse(result.Contains(new Package("Michael Opitz", "Gallbrunn", "2463", "Dresdner Strasse 3")));
		}

		[TestMethod]
		public void GetByStreetAndPostcode_ListOfPackages_Test()
		{
			string toSearch = "Dresdner Strasse 3";
			List<Package> expectedResult = new List<Package> 
						{ 
							new Package("Dawn Alolino", "Wien", "1200", "Dresdner Strasse 3"),
						};
			List<Package> result = new List<Package>(iMPackage.GetByStreetAndPostcode("1200", toSearch));

			Assert.AreEqual(result[0].Address.Postcode, expectedResult[0].Address.Postcode);
			Assert.IsFalse(result.Contains(new Package("Michael Opitz", "Gallbrunn", "2463", "Dresdner Strasse 3")));
		}

		[TestMethod]
		public void GetByAddress_ListOfPackages_Test()
		{
			Address toSearch = new Address("Wien", "1200", "Dresdner Strasse 3");
			List<Package> expectedResult = new List<Package> 
						{ 
							new Package("Dawn Alolino", "Wien", "1200", "Dresdner Strasse 3"),
						};
			List<Package> result = new List<Package>(iMPackage.GetByAddress(toSearch));

			Assert.AreEqual(result[0].Address.Location, expectedResult[0].Address.Location);
			Assert.IsFalse(result.Contains(new Package("Michael Opitz", "Gallbrunn", "2463", "Dresdner Strasse 3")));
		}

		[TestMethod]
		public void Add_AddNewPackage_Test()
		{
			Package toAdd = new Package("TestPackage", "Wien", "1200", "Dresdner Strasse 5");

			iMPackage.Add(toAdd);

			Assert.AreEqual(iMPackage.PackageList[3].Recipient, toAdd.Recipient);
			Assert.IsNotNull(iMPackage.PackageList[3]);
		}

		[TestMethod]
		public void Update_UpdatePackage_Test()
		{
			Package toUpdate = new Package("TestPackage", "Wien", "1200", "Dresdner Strasse 5");

			iMPackage.Update(iMPackage.PackageList[0], toUpdate);

			Assert.AreNotEqual(iMPackage.PackageList[0].Recipient, "Dawn Alolino");
			Assert.IsNotNull(iMPackage.PackageList[0]);
		}

		[TestMethod]
		public void Delete_DeletePackage_Test()
		{
			Package toDelete = iMPackage.PackageList[0];

			iMPackage.Delete(toDelete);

			Assert.AreNotEqual(iMPackage.PackageList[0].Recipient, toDelete.Recipient);
		}

		[TestMethod]
		public void GetAll_GetAllPackage_Test()
		{
			List<Package> result;
			result = new List<Package>(iMPackage.GetAll());

			Assert.IsNotNull(result);
			Assert.AreEqual(result[0].Recipient, iMPackage.PackageList[0].Recipient);
		}

		[TestMethod]
		public void GetPackages_GetPackagesByRegionKey_Test()
		{
			// Arrange
			List<Package> result;

			// Act
			result = new List<Package>(iMPackage.GetByRegionKey("W1200"));

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(result[0].Recipient, iMPackage.PackageList[0].Recipient);
		}

		[TestMethod]
		public void Deliver_DeliverPackagesFromRegion_Test()
		{
			// Arrange
			List<Package> result;

			// Act
			result = new List<Package>(iMPackage.DeliverPackagesFromRegion("W1200"));

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(result[0].Recipient, iMPackage.PackageList[0].Recipient);
			Assert.IsTrue(result[0].Delivered);
			Assert.IsTrue(iMPackage.PackageList[0].Delivered);
		}

		[TestMethod]
		public void NotDelivered_GetDeliveredPackagesByRegionKey_Test()
		{
			// Arrange
			List<Package> result;

			// Act
			result = new List<Package>(iMPackage.GetDeliveredPackagesByRegionKey("W1200", false));

			// Assert
			Assert.IsNotNull(result);
			Assert.AreEqual(result[0].Recipient, iMPackage.PackageList[0].Recipient);
			Assert.IsFalse(result[0].Delivered);
		}

		[TestMethod]
		public void Delivered_GetDeliveredPackagesByRegionKey_Test()
		{
			// Arrange
			List<Package> result;

			// Act
			result = new List<Package>(iMPackage.GetDeliveredPackagesByRegionKey("W1200", true));

			// Assert
			Assert.IsNull(result.FirstOrDefault());
		}
	}
}

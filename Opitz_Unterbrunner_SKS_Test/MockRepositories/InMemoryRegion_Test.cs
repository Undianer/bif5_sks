﻿using System.Collections.Generic;
using DAL.Mock;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Opitz_Unterbrunner_SKS_Test.MockRepositories
{
    [TestClass]
    public class InMemoryRegion_Test
    {
       public List<Region> regionList;
       public InMemoryRegion iMR;

        public InMemoryRegion_Test()
        {
            regionList = new List<Region>();
            regionList.Add(new Region("Wien"));
            regionList.Add(new Region("Gallbrunn"));
            regionList.Add(new Region("Fucking"));
            iMR = new InMemoryRegion();
            iMR.RegList = regionList;
        }

        [TestMethod]
        public void Add_ListOfRegionsTest()
        {
            //Arrange
            Region reg = new Region("St.Veit");
            
            //Act
            iMR.Add(reg);

            //Assert
            Assert.IsNotNull(iMR.RegList);
            Assert.IsTrue(iMR.RegList.Contains(reg));
        }
        [TestMethod]
        public void Update_ListOfRegionsTest()
        {
            Region expectedResult = new Region("Trolol");
            iMR.Update(regionList[0], expectedResult);

            Assert.IsNotNull(iMR.RegList);
            Assert.AreNotSame(regionList.Count, iMR.RegList.Count);
            Assert.IsTrue(iMR.RegList.Contains(expectedResult));
        }
        [TestMethod]
        public void Delete_ListOfRegionsTest()
        {
            Region reg = new Region("Mariahilf");
            iMR.Delete(reg);

            Assert.AreNotSame(iMR.RegList.Count, regionList.Count);
            Assert.IsFalse(iMR.RegList.Contains(reg));
        }
        [TestMethod]
        public void GetAll_ListOfRegionsTest()
        {
            List<Region> result = new List<Region>();
            result = iMR.RegList;

            Assert.AreEqual(result, iMR.GetAll());
        }
        [TestMethod]
        public void GetByName_ListOfRegionsTest()
        {
            string toSearch = "Wien";

            List<Region> expectedResult = new List<Region>
            {
                new Region("Wien")
            };

            List<Region> result = new List<Region>(iMR.GetByName(toSearch));

            Assert.AreEqual(result[0].Name, expectedResult[0].Name);
            Assert.AreEqual(result.Count, expectedResult.Count);
        
        }
    }
}

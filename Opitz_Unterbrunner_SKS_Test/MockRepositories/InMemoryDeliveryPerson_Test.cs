﻿using System.Collections.Generic;
using DAL.Mock;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Opitz_Unterbrunner_SKS_Test.MockRepositories
{
	[TestClass]
	public class InMemoryDeliveryPerson_Test
	{
		List<DeliveryPerson> deliveryPersonList;
		InMemoryDeliveryPerson iMdP;


		public InMemoryDeliveryPerson_Test()
		{
			deliveryPersonList = new List<DeliveryPerson>();
			deliveryPersonList.Add(new DeliveryPerson(1, "Stefan Opitz"));
			deliveryPersonList.Add(new DeliveryPerson(2, "Stefan Opitz"));
			deliveryPersonList.Add(new DeliveryPerson(3, "Andreas Unterbrunner"));
			deliveryPersonList.Add(new DeliveryPerson(4, "Damiano Neufeld"));
			iMdP = new InMemoryDeliveryPerson();
			iMdP.DeliveryPersonList = deliveryPersonList;
		}

		[TestMethod]
		public void GetByName_ListOfDelPersons_Test()
		{
			List<DeliveryPerson> expectedResult = new List<DeliveryPerson>();
			expectedResult.Add(new DeliveryPerson(1, "Stefan Opitz"));
			expectedResult.Add(new DeliveryPerson(2, "Stefan Opitz"));
			List<DeliveryPerson> result;


			result = new List<DeliveryPerson>(iMdP.GetByName("Stefan Opitz"));

			Assert.IsNotNull(result);
			Assert.AreNotSame(deliveryPersonList, result);
			Assert.AreEqual(expectedResult.Count, result.Count);
			Assert.AreSame(result.ToString(), expectedResult.ToString());


		}
		[TestMethod]
		public void GetBySvn_ListOfDelPersons_Test()
		{
			DeliveryPerson expectedResult = new DeliveryPerson(1, "Stefan Opitz");

			DeliveryPerson result = iMdP.GetBySvn(1);

			Assert.IsNotNull(result);
			Assert.AreNotSame(deliveryPersonList, result);
			Assert.AreEqual(expectedResult.Svn, result.Svn);
			Assert.AreSame(result.Name, expectedResult.Name);


		}
		[TestMethod]
		public void Add_ListOfDelPersons_Test()
		{
			DeliveryPerson expectedResult = new DeliveryPerson(5, "Blitz aus Pitz");
			iMdP.Add(expectedResult);

			Assert.IsNotNull(iMdP.DeliveryPersonList);
			Assert.AreNotSame(deliveryPersonList.Count, iMdP.DeliveryPersonList);
			Assert.IsTrue(iMdP.DeliveryPersonList.Contains(expectedResult));


		}
		[TestMethod]
		public void Update_ListOfDelPersons_Test()
		{
			DeliveryPerson expectedResult = new DeliveryPerson(1, "Stefan Pitz");
			iMdP.Update(deliveryPersonList[0], expectedResult);

			Assert.IsNotNull(iMdP.DeliveryPersonList);
			Assert.AreNotSame(deliveryPersonList.Count, iMdP.DeliveryPersonList);
			Assert.IsTrue(iMdP.DeliveryPersonList.Contains(expectedResult));


		}
		[TestMethod]
		public void Delete_ListOfDelPersons_Test()
		{
			DeliveryPerson expectedResult = new DeliveryPerson(1, "Stefan Pitz");
			iMdP.Delete(expectedResult);


			Assert.AreNotSame(deliveryPersonList.Count, iMdP.DeliveryPersonList.Count);
			Assert.IsFalse(iMdP.DeliveryPersonList.Contains(expectedResult));
		}
		[TestMethod]
		public void GetAll_ListOfDelPersons_Test()
		{
			List<DeliveryPerson> result = new List<DeliveryPerson>();
			result = iMdP.DeliveryPersonList;

			Assert.AreEqual(result, iMdP.GetAll());
		}
	}
}

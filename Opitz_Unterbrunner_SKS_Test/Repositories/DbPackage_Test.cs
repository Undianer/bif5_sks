﻿using System.Collections.Generic;
using System.Linq;
using DAL.OutMemory;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Opitz_Unterbrunner_SKS_Test.Repositories
{
	/// <summary>
	/// Zusammenfassungsbeschreibung für DbPackage_Test
	/// </summary>
	[TestClass]
	public class DbPackage_Test
	{
		private IPackageRepo PackageRepo { get; set; }
        private readonly log4net.ILog log;


		public DbPackage_Test()
		{
         //   log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(typeof(DbPackage_Test));
          //  log.Info("------------------------------------------------------------------------------");
			PackageRepo = new DbPackageRepo();
		}

		#region Zusätzliche Testattribute
		//
		// Sie können beim Schreiben der Tests folgende zusätzliche Attribute verwenden:
		//
		// Verwenden Sie ClassInitialize, um vor Ausführung des ersten Tests in der Klasse Code auszuführen.
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Verwenden Sie ClassCleanup, um nach Ausführung aller Tests in einer Klasse Code auszuführen.
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		#endregion

		private void deleteDb()
		{
			using (var db = new EntityContext())
			{
				db.Database.Delete();
			}
		}

		// Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
		[TestInitialize()]
		public void MyTestInitialize()
		{
			deleteDb();
            log.Info("Datenbank gelöscht");
		}

		//Mit TestCleanup können Sie nach jedem einzelnen Test Code ausführen.
		[TestCleanup()]
		public void MyTestCleanup()
		{
			deleteDb();
            log.Info("Datenbank gelöscht");
		}

		[TestMethod]
		public void Add_PackageToDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 75, 75, "Bruck an der Leitha");
			int expectedResult = 1;
			int result;

			// Act
            try
            {
                PackageRepo.Add(toAdd);
            }
            catch (RepositoryException)
            {
                log.Error("Test_Add_PackageToDb fehlgeschlagen");
            }
			
			// Assert
			using (var db = new EntityContext())
			{
				result = db.Packages.ToList().Count;
			}

			Assert.IsTrue(expectedResult == result);
			Assert.IsFalse(result < 1);
            log.Info("AddPackage() getestet");
		}

		[TestMethod]
		public void Update_PackageInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 75, 75, "Bruck an der Leitha");
			Package toChange = toAdd;
			toChange.Recipient = "Andreas Unterbrunner";
			string expectedResult = "Andreas Unterbrunner";
			string result;

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Update(null, toChange);
            }
            catch (RepositoryException)
            {
                log.Error("Update fehlgeschlagen");
            }
			
			// Assert
			using (var db = new EntityContext())
			{
				result = db.Packages.Find(toChange.Id).Recipient;
			}

			Assert.AreEqual(expectedResult, result);
			Assert.IsNotNull(result);
            log.Info("UpdatePackage() getestet");
		}

		[TestMethod]
		public void Delete_PackageInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 75, 75, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse", 75, 75, "Wien");
			int expectedResult = 1;
			int result;

			// Act

            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                PackageRepo.Delete(toAdd2);
            }
            catch (RepositoryException)
            {
                log.Error("Test_Delete_PackageToDb fehlgeschlagen");
            }
			
			// Assert
			using (var db = new EntityContext())
			{
				result = db.Packages.ToList().Count;
			}

			Assert.IsTrue(expectedResult == result);
			Assert.IsNotNull(result);
			Assert.IsTrue(result < 2);
            log.Info("DeletePackage() getestet");
		}

		[TestMethod]
		public void GetAll_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 75, 75, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse", 75, 75, "Wien");
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2 };
			List<Package> result = new List<Package>();

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                result = (List<Package>) PackageRepo.GetAll();
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetAll_PackageToDb fehlgeschlagen");
            }
			
			// Assert
			Assert.IsTrue(expectedResult.Count == result.Count);
			Assert.IsNotNull(result);
			Assert.AreEqual(expectedResult[0].Recipient, result[0].Recipient);
            log.Info("GetAllPackage() getestet");
		}

		[TestMethod]
		public void GetByLocation_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			List<Package> result = new List<Package>();

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                PackageRepo.Add(toAdd3);
                result = (List<Package>) PackageRepo.GetByLocation("Wien");
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetByLocation_PackageToDb fehlgeschlagen");
            }

			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count > 1);
			Assert.AreNotEqual(expectedResult[0].Address.Location, result[1].Address.Location);
			Assert.AreEqual("Wien", result[0].Address.Location);
            log.Info("GetByLocationPackage() getestet");
		}

		[TestMethod]
		public void GetByPostcode_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			List<Package> result = new List<Package>();

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                PackageRepo.Add(toAdd3);
                result = (List<Package>) PackageRepo.GetByPostcode("1100");
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetByPostcode_PackageToDb fehlgeschlagen");
            }
			
			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count > 1);
			Assert.IsFalse(expectedResult[0].Address.Postcode == result[1].Address.Postcode);
			Assert.IsTrue("1100".Equals(result[0].Address.Postcode));
            log.Info("GetByPostcodePackage() getestet");
		}

		[TestMethod]
		public void GetByStreetAndLocation_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			List<Package> result = new List<Package>();

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                PackageRepo.Add(toAdd3);
                result = (List<Package>) PackageRepo.GetByStreet("Wien", "Whoknows 12");
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetByStreetAndLocation_PackageToDb fehlgeschlagen");
            }
			
			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 1);
			Assert.AreEqual(expectedResult[2].Address.Location, result[0].Address.Location);
			Assert.AreEqual(expectedResult[2].Address.Street, result[0].Address.Street);
            log.Info("GetByStreetPackage() getestet");
		}

		[TestMethod]
		public void GetByStreetAndPostcode_PackagesInDb_Success()
		{
			// Arrange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			List<Package> result = new List<Package>();

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                PackageRepo.Add(toAdd3);
                result = (List<Package>)PackageRepo.GetByStreetAndPostcode("1100", "Whoknows 12");
            }
            catch (RepositoryException ex)
            {
                log.Error("Test_GetByStreetAndPostcode_PackageToDb fehlgeschlagen", ex);
            }
			
			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count >= 1);
			Assert.AreEqual(expectedResult[2].Address.Street, result[0].Address.Street);
			//Assert.IsTrue(expectedResult[2].Address.Postcode.Equals(result[0].Address.Postcode));
			//Assert.IsTrue("1100".Equals(result[0].Address.Postcode));
            log.Info("GetByStreetAndPostcodePackage() getestet");
		}

		[TestMethod]
		public void GetByAddress_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			Address toSearch = toAdd.Address;
			List<Package> result = new List<Package>();

			// Act
            try
            {
                PackageRepo.Add(toAdd);
                PackageRepo.Add(toAdd2);
                PackageRepo.Add(toAdd3);
                result = (List<Package>) PackageRepo.GetByAddress(toSearch);
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetByAddress_PackageToDb fehlgeschlagen");
            }
			
			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 1);
			Assert.IsTrue(result[0].Address.Equals(expectedResult[0].Address));
            log.Info("GetByAddressPackage() getestet");
		}

		[TestMethod]
		public void GetByRegionKey_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			Region testRegion = new Region("W1200", "Wien 20", "Wien", "1200", "Dresdnerstrasse");
			toAdd2.Region = testRegion;
			toAdd3.Region = testRegion;
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			Address toSearch = toAdd.Address;
			List<Package> result = null;

			try
			{
				using (var db = new EntityContext())
				{
					db.Packages.Add(toAdd);
					db.Packages.Add(toAdd2);
					db.Packages.Add(toAdd3);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				log.Error("Test_GetByRegionKey Arrange fehlgeschlagen");
			}

			// Act
			try
			{
				result = new List<Package>(PackageRepo.GetByRegionKey(testRegion.Key));
			}
			catch (RepositoryException)
			{
				log.Error("Test_GetByAddress_PackageToDb Act fehlgeschlagen");
			}

			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count >= 1);
			Assert.IsTrue(result[0].Region.Key.Equals(testRegion.Key));
			log.Info("GetByAddressPackage() getestet");
		}

		[TestMethod]
		public void DeliverPackagesFromRegion_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			Region testRegion = new Region("W1200", "Wien 20", "Wien", "1200", "Dresdnerstrasse");
			toAdd2.Region = testRegion;
			toAdd3.Region = testRegion;
			toAdd3.Delivered = true;
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			Address toSearch = toAdd.Address;
			List<Package> result = new List<Package>();

			try
			{
				using (var db = new EntityContext())
				{
					db.Packages.Add(toAdd);
					db.Packages.Add(toAdd2);
					db.Packages.Add(toAdd3);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				log.Error("Test_DeliverPackagesFromRegion Arrange fehlgeschlagen");
			}

			// Act
			try
			{
				result = new List<Package>(PackageRepo.DeliverPackagesFromRegion(testRegion.Key));
			}
			catch (RepositoryException)
			{
				log.Error("Test_DeliverPackagesFromRegion Act fehlgeschlagen");
			}

			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 1);
			Assert.IsTrue(result[0].Region.Key.Equals(testRegion.Key));
			Assert.IsTrue(result[0].Delivered);
			log.Info("DeliverPackagesFromRegion getestet");
		}

		[TestMethod]
		public void GetDeliveredByRegionKey_PackagesInDb_Success()
		{
			// Arange
			Package toAdd = new Package("Stefan Opitz", "Gallbrunn", "2463", "Hauptstrasse 75", 12, 12, "Bruck an der Leitha");
			Package toAdd2 = new Package("Andreas Unterbrunner", "Wien", "1100", "Laengenfeldgasse 12", 75, 75, "Wien");
			Package toAdd3 = new Package("Damiano Neufeld", "Wien", "1100", "Whoknows 12", 75, 75, "Wien");
			Region testRegion = new Region("W1200", "Wien 20", "Wien", "1200", "Dresdnerstrasse");
			toAdd2.Region = testRegion;
			toAdd3.Region = testRegion;
			List<Package> expectedResult = new List<Package> { toAdd, toAdd2, toAdd3 };
			Address toSearch = toAdd.Address;
			List<Package> result = null;

			try
			{
				using (var db = new EntityContext())
				{
					db.Packages.Add(toAdd);
					db.Packages.Add(toAdd2);
					db.Packages.Add(toAdd3);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
				log.Error("Test_GetByRegionKey Arrange fehlgeschlagen");
			}

			// Act
			try
			{
				result = new List<Package>(PackageRepo.GetDeliveredPackagesByRegionKey(testRegion.Key, false));
			}
			catch (RepositoryException)
			{
				log.Error("Test_GetByAddress_PackageToDb Act fehlgeschlagen");
			}

			// Assert
			Assert.AreNotSame(expectedResult.Count, result.Count);
			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count >= 1);
			Assert.IsTrue(result[0].Region.Key.Equals(testRegion.Key));
			Assert.IsFalse(result[0].Delivered);
			log.Info("GetByAddressPackage() getestet");
		}
	}
}

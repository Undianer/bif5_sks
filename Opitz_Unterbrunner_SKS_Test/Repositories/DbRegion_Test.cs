﻿using System.Collections.Generic;
using System.Linq;
using DAL.OutMemory;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;

namespace Opitz_Unterbrunner_SKS_Test.Repositories
{

    [TestClass]
    public class DbRegion_Test
    {
        private IRegionRepo RegionRepo { get; set; }

        private readonly log4net.ILog log;

        public DbRegion_Test()
        {
            log = log4net.LogManager.GetLogger(typeof(DbRegion_Test));

            RegionRepo = new DbRegionRepo();
        }

        #region Zusätzliche Testattribute
        //
        // Sie können beim Schreiben der Tests folgende zusätzliche Attribute verwenden:
        //
        // Verwenden Sie ClassInitialize, um vor Ausführung des ersten Tests in der Klasse Code auszuführen.
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Verwenden Sie ClassCleanup, um nach Ausführung aller Tests in einer Klasse Code auszuführen.
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        //
        #endregion

        private void deleteDb()
        {
            using (var db = new EntityContext())
            {
				db.Database.ExecuteSqlCommand("delete from dbo.Packages");
				db.Database.ExecuteSqlCommand("delete from dbo.Regions");
                db.Database.ExecuteSqlCommand("delete from dbo.Addresses");
                db.Database.ExecuteSqlCommand("delete from dbo.Coords");
            }
        }

        // Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
        [TestInitialize()]
        public void MyTestInitialize()
        {
            deleteDb();
            log.Info("Datenbank gelöscht");
        }

        //Mit TestCleanup können Sie nach jedem einzelnen Test Code ausführen.
        [TestCleanup()]
        public void MyTestCleanup()
        {
            deleteDb();
            log.Info("Datenbank gelöscht");
        }

        [TestMethod]
        public void Add_RegionToDb_Success()
        {
            // Arange
            Region toAdd = new Region("R1200","Wien", "Wien", "1200", "Laengenfeldgasse 12");
            toAdd.AddPackage(new Package("Andreas Unterbrunner", "Wien", "1200", "Laengenfeldgasse 12"));
            toAdd.PackageList[0].Coords = new Coords(12, 12);
            int expectedResult = 1;
            int result;

            // Act
            try
            {
                RegionRepo.Add(toAdd);
            }
            catch (RepositoryException ex)
            {
                log.Error("Test_Add_RegionToDb fehlgeschlagen!", ex);
            }

            // Assert
            using (var db = new EntityContext())
            {
                result = db.Regions.ToList().Count;
            }

            Assert.IsTrue(expectedResult == result);
            Assert.IsFalse(result < 1);
            log.Info("AddRegion() getestet");
        }

        [TestMethod]
        public void Update_RegionInDb_Success()
        {
            // Arange
            Region toAdd = new Region("W1200", "Wien Bezirk 20", "Wien", "1200", "Hochstädtplatz 4");
            toAdd.AddPackage(new Package("Andreas Unterbrunner", "Wien", "1200", "Laengenfeldgasse 12"));
            toAdd.PackageList[0].Coords = new Coords(12, 12);
            Region toChange = toAdd;
            toChange.Name = "Linz";
	        toChange.Address.Street = "Höchstädtplatz 5";
            string expectedResult = "Linz";
	        string notExpectedResult = "Hochstädtplatz 4";
            Region result;

			using (var db = new EntityContext())
			{
				db.Regions.Add(toAdd);
				db.SaveChanges();
			}

            // Act
            try
            {
                RegionRepo.Update(null, toChange);
            }
            catch (RepositoryException)
            {
                log.Error("Test_Update_RegionToDb fehlgeschlagen!");
            }

            // Assert
            using (var db = new EntityContext())
            {
                result = db.Regions.Include(r => r.Address).FirstOrDefault(r => r.Key == toChange.Key);
            }

			Assert.IsNotNull(result);
			Assert.AreEqual(expectedResult, result.Name);
			Assert.AreNotEqual(result.Address.Street, notExpectedResult);
            log.Info("UpdateRegion() getestet");
        }

        [TestMethod]
        public void Delete_RegionInDb_Success()
        {
            // Arange
            Region toAdd = new Region("Wien");
            toAdd.AddPackage(new Package("Andreas Unterbrunner", "Wien", "1200", "Laengenfeldgasse 12"));
            toAdd.PackageList[0].Coords = new Coords(12, 12);
            Region toAdd2 = new Region("Linz");
            toAdd2.AddPackage(new Package("Stefan Opitz", "Linz", "3341", "Teststrasse 12"));
            toAdd2.PackageList[0].Coords = new Coords(12, 12);
            int expectedResult = 1;
            int result;

            // Act
            try
            {
                RegionRepo.Add(toAdd);
                RegionRepo.Add(toAdd2);
                RegionRepo.Delete(toAdd2);
            }
            catch (RepositoryException)
            {
                log.Error("Test_Delete_RegionToDb fehlgeschlagen!");
            }

            // Assert
            using (var db = new EntityContext())
            {
                result = db.Regions.ToList().Count;
            }

            Assert.IsTrue(expectedResult == result);
            Assert.IsNotNull(result);
            Assert.IsTrue(result < 2);
            log.Info("DeleteRegion() getestet");
        }

        [TestMethod]
        public void GetAll_RegionsInDb_Success()
        {
            // Arange
            Region toAdd = new Region("Wien");
            toAdd.AddPackage(new Package("Andreas Unterbrunner", "Wien", "1200", "Laengenfeldgasse 12"));
            toAdd.PackageList[0].Coords = new Coords(12, 12);
            Region toAdd2 = new Region("Linz");
            toAdd2.AddPackage(new Package("Stefan Opitz", "Linz", "3341", "Teststrasse 12"));
            toAdd2.PackageList[0].Coords = new Coords(12, 12);
            List<Region> expectedResult = new List<Region> { toAdd, toAdd2 };
            List<Region> result = new List<Region>();
            // Act
            try
            {
                RegionRepo.Add(toAdd);
                RegionRepo.Add(toAdd2);
                result = (List<Region>)RegionRepo.GetAll();
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetAll_RegionToDb fehlgeschlagen!");
            }

            // Assert
            Assert.IsTrue(expectedResult.Count == result.Count);
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedResult[0].Name, result[0].Name);
            log.Info("GetAllRegion() getestet");
        }

        [TestMethod]
        public void GetByName_RegionsInDb_Success()
        {
            // Arange
            Region toAdd = new Region("Wien");
            toAdd.AddPackage(new Package("Andreas Unterbrunner", "Wien", "1200", "Laengenfeldgasse 12"));
            toAdd.PackageList[0].Coords = new Coords(12, 12);
            Region toAdd2 = new Region("Linz");
            toAdd2.AddPackage(new Package("Stefan Opitz", "Linz", "3341", "Teststrasse 12"));
            toAdd2.PackageList[0].Coords = new Coords(12, 12);
            Region toAdd3 = new Region("Wien");
            toAdd3.AddPackage(new Package("Damiano Neufeld", "Wien", "1200", "Whoknows 12"));
            toAdd3.PackageList[0].Coords = new Coords(12, 12);
            List<Region> expectedResult = new List<Region> { toAdd, toAdd2, toAdd3 };
            List<Region> result = new List<Region>();

            // Act
            try
            {
                RegionRepo.Add(toAdd);
                RegionRepo.Add(toAdd2);
                RegionRepo.Add(toAdd3);
                result =  (List<Region>) RegionRepo.GetByName("Wien");
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetByName_RegionToDb fehlgeschlagen!");
            }
            
            // Assert
            Assert.AreNotSame(expectedResult.Count, result.Count);
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedResult[0].Name, result[0].Name);
            Assert.AreEqual("Wien", result[0].Name);
            log.Info("GetByNameRegion() getestet");
        }
    }
}

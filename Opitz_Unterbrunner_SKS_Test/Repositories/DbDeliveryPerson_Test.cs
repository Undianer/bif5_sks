﻿using System.Collections.Generic;
using System.Linq;
using DAL.OutMemory;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Opitz_Unterbrunner_SKS_Test.Repositories
{
    /// <summary>
    /// Zusammenfassungsbeschreibung für DbDeliveryPerson_Test
    /// </summary>
    [TestClass]
    public class DbDeliveryPerson_Test
    {
        private IDeliveryPersonRepo DeliveryPersonRepo { get; set; }
        private readonly log4net.ILog log;

        public DbDeliveryPerson_Test()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(typeof(DbDeliveryPerson_Test));
            DeliveryPersonRepo = new DbDeliveryPersonRepo();
        }



        #region Zusätzliche Testattribute
        //
        // Sie können beim Schreiben der Tests folgende zusätzliche Attribute verwenden:
        //
        // Verwenden Sie ClassInitialize, um vor Ausführung des ersten Tests in der Klasse Code auszuführen.
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Verwenden Sie ClassCleanup, um nach Ausführung aller Tests in einer Klasse Code auszuführen.
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private void deleteDb()
        {

            using (var db = new EntityContext())
            {
				db.Database.ExecuteSqlCommand("delete from dbo.DeliveryPersons");
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            deleteDb();
            log.Info("Datenbank gelöscht");
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            deleteDb();
            log.Info("Datenbank gelöscht");
        }

        [TestMethod]
        public void GetBySvn_DeliveryPersonInDb_True()
        {
            //Arrange
            DeliveryPerson expectedResult = new DeliveryPerson(1, "Stefan Opitz");
            DeliveryPerson result = new DeliveryPerson();
            using (var db = new EntityContext())
            {
                db.DeliveryPersons.Add(expectedResult);
                db.SaveChanges();
            }

            //Act
            try
            {
                result = DeliveryPersonRepo.GetBySvn(1);
            }
            catch (RepositoryException)
            {
                log.Error("Test_GetBySvn_DeliveryPerson fehlgeschlagen");
            }
           
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Name, expectedResult.Name);
            log.Info("GetBySvnPerson() getestet");
        }

        [TestMethod]
        public void Add_DeliveryPersonInDb_True()
        {
            //Arrange
            DeliveryPerson toAdd = new DeliveryPerson(1, "TestPerson");
            int expectedResult = 1;
            int result;

            //Act
            try
            {
                DeliveryPersonRepo.Add(toAdd);

            }
            catch (RepositoryException)
            {
                log.Error("Test_Add_DeliveryPerson fehlgeschlagen");
            }
            

            //Assert
            using (var db = new EntityContext())
            {
                result = db.DeliveryPersons.ToList().Count();
            }
            Assert.AreEqual(result, expectedResult);
            Assert.IsTrue(result == expectedResult);
            log.Info("AddPerson() getestet");
        }

        [TestMethod]
        public void Update_DeliveryPersonInDb()
        {
            //Arrange
            DeliveryPerson oldEntry = new DeliveryPerson(55, "Ich bin alt");
            using (var db = new EntityContext())
            {
                db.DeliveryPersons.Add(oldEntry);
                db.SaveChanges();
            }
            string entryName = "Ich bin neu";
            oldEntry.Name = entryName;
            string result;

            //Act
            try
            {
                DeliveryPersonRepo.Update(null, oldEntry);
            }
            catch (RepositoryException)
            {
                log.Error("Test_Update_DeliveryPerson fehlgeschlagen");
            }
            
            //Assert
            using (var db = new EntityContext())
            {
                result = db.DeliveryPersons.Find(oldEntry.Id).Name;
            }
            Assert.AreEqual(result, entryName);
            Assert.IsNotNull(result);
            log.Info("UpdatePerson() getestet");
        }

        [TestMethod]
        public void Delete_DeliveryPersonInDb()
        {
            //Arrange
            DeliveryPerson notToDelete = new DeliveryPerson(40, "Ich werde leben");
            DeliveryPerson toDelete = new DeliveryPerson(40, "Ich werde sterben");
            List<DeliveryPerson> result = new List<DeliveryPerson>();
            int resultCount;
            using (var db = new EntityContext())
            {
                db.DeliveryPersons.Add(notToDelete);
                db.DeliveryPersons.Add(toDelete);
                db.SaveChanges();
            }

            //Act 
            try
            {
                DeliveryPersonRepo.Delete(toDelete);
            }
            catch (RepositoryException)
            {
                log.Error("Test_Delete_DeliveryPerson fehlgeschlagen");
            }
            
            
            //Assert
            using (var db = new EntityContext())
            {
                result = db.DeliveryPersons.ToList();
                resultCount = db.DeliveryPersons.ToList().Count();
            }
            Assert.AreNotEqual(result[0].Name, "Ich werde sterben");
            Assert.IsTrue(resultCount == 1);
            log.Info("DeletePerson() getestet");
        }

    }
}
﻿using GeodataService;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Opitz_Unterbrunner_SKS_Test.GeodataAgent
{
    [TestClass]
    public class GeodataServiceMock_Test
    {
        double[] TestCoords;
        double[] MockCoords;
        GeodataServiceMock mock;

        public GeodataServiceMock_Test()
        {
            TestCoords = new double[2];
            mock = new GeodataServiceMock();
            MockCoords = mock.MockCoords;
        }

        [TestMethod]
        public void makeRequest_Test()
        {
            TestCoords = new double[2];
            TestCoords = mock.MakeRequest();

            Assert.AreEqual(TestCoords[0], MockCoords[0]);
            Assert.AreEqual(TestCoords[1], MockCoords[1]);
        }
    }
}

﻿using EntityRepo.Entities;
using GeodataService;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Opitz_Unterbrunner_SKS_Test.GeodataAgent
{
    [TestClass]
    public class GeodataServiceAgent_Test
    {
        private GeodataServiceAgent agent;
        double[] result;
        Address adr;
        private static readonly ILog log = LogManager.GetLogger(global::System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GeodataServiceAgent_Test()
        {
            result = new double[2];
            agent = new GeodataServiceAgent();
            adr = new Address("Wien", "1120", "Längenfeldgasse");
        }
        [TestMethod]
        public void EncodeCoordinates_Test()
        {
            log.Info("GeodataServiceAgent_Test.EncodeCoordinates_Test gestartet");
            result = agent.EncodeCoordinates(adr);

            Assert.AreEqual(2, result.Length);
        }
    }
}

﻿using System.Collections.Generic;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestService;

namespace Opitz_Unterbrunner_SKS_Test.RegionImport
{
	/// <summary>
	/// Zusammenfassungsbeschreibung für RegionMapper_Test
	/// </summary>
	[TestClass]
	public class RegionMapper_Test
	{
		public RegionData Data { get; set; }
		public List<Region> RegionList { get; set; }

		public RegionMapper_Test()
		{
			Data = new RegionData
			{
				Region = new[]
                {
                    new RegionDataRegion()
                    {
                        Key = "W1170",
                        DisplayName = "Wien_1170",
                        Address = new RegionDataRegionAddress()
                        {
                            City = "Wien",
                            PostalCode = "A-1170",
                            Street = "Wattgasse 56-60"
                        }
                    },
                    new RegionDataRegion()
                    {
                        Key = "W1200",
                        DisplayName = "Wien_1200",
                        Address = new RegionDataRegionAddress()
                        {
                            City = "Wien",
                            PostalCode = "A-1200",
                            Street = "Dresdner Straße 116-118"
                        }
                    },
                    new RegionDataRegion()
                    {
                        Key = "W1100",
                        DisplayName = "Wien_1100",
                        Address = new RegionDataRegionAddress()
                        {
                            City = "Wien",
                            PostalCode = "A-1100",
                            Street = "Buchengasse 77"
                        }
                    }
                }
			};

			RegionList = new List<Region>()
			{
				new Region("W1170", "Wien_1170", "Wien", "A-1170", "Wattgasse 56-60"),
				new Region("W1200", "Wien_1200", "Wien", "A-1200", "Dresdner Straße 116-118"),
				new Region("W1100", "Wien_1100", "Wien", "A-1100", "Buchengasse 77")
			};
		}

		private TestContext testContextInstance;

		/// <summary>
		///Ruft den Textkontext mit Informationen über
		///den aktuellen Testlauf sowie Funktionalität für diesen auf oder legt diese fest.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		[TestMethod]
		public void MapRegionDataToRegion_Success()
		{
			// Arrange
			RegionMapper mapper = new RegionMapper();
			List<Region> result;

			// Act
			result = mapper.MapRegionDataToRegion(Data);

			// Assert
			Assert.IsTrue(result.Count == Data.Region.Length);
		}

		[TestMethod]
		public void MapRegionListToRegionData_Success()
		{
			// Arrange
			RegionMapper mapper = new RegionMapper();
			RegionData result;

			// Act
			result = mapper.MapRegionListToRegionData(RegionList);

			// Assert
			Assert.IsTrue(result.Region.Length == RegionList.Count);
		}
	}
}

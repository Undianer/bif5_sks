﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL.OutMemory;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo.Entities;
using EntityRepo;
using Microsoft.Practices.Unity;

namespace opun.sks.Website.Controllers
{
    public class RegionController : Controller
    {
        private IRegionRepo regionRepo;
	    public IUnityContainer UnityContainer { get; set; }	

	    public RegionController()
	    {
		    UnityContainer = new UnityContainer();
		    UnityContainer.RegisterType<IRegionRepo, DbRegionRepo>();
		    regionRepo = UnityContainer.Resolve<IRegionRepo>();
	    }

        // GET: /Default1/
        public ActionResult Index()
        {
			List<Region> regions = new List<Region>(regionRepo.GetAll());
		    return View(regions);
        }

        // GET: /Default1/Upload
        public ActionResult Upload()
        {
            return View();
        }

        // POST: /Default1/Upload
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
		public ActionResult Upload(HttpPostedFileBase file)
        {
	        if (file.ContentLength > 0)
	        {
				//TODO: Access the RegionUploadService to upload xml file
	        }

	        return RedirectToAction("Index");
            return View();
        }

        // GET: /Default1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

	        Region region = regionRepo.GetById(id.Value);

            if (region == null)
            {
                return HttpNotFound();
            }
            return View(region);
        }

        // POST: /Default1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Key,Name")] Region region)
        {
            if (ModelState.IsValid)
            {
                regionRepo.Update(null, region);
                return RedirectToAction("Index");
            }
            return View(region);
        }

        // GET: /Default1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Region region = regionRepo.GetById(id.Value);
            if (region == null)
            {
                return HttpNotFound();
            }
            return View(region);
        }

        // POST: /Default1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Region region = regionRepo.GetById(id);
            regionRepo.Delete(region);
            return RedirectToAction("Index");
        }
    }
}

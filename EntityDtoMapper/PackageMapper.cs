﻿using EntityRepo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityDtoMapper
{
    public class PackageMapper
    {
        public Package MapDtoToPackage(schema.skspackage.org._2013.ShippingService.Package dto)
        {
            Package package = new Package
            (
                "Anonymous",
                dto.Address.City,
                dto.Address.PostalCode,
                dto.Address.Street
            );

            return package;
        }

        public schema.skspackage.org._2013.DeliveryService.Package MapPackageToDto(Package entity)
        {
            schema.skspackage.org._2013.DeliveryService.Package dto = new schema.skspackage.org._2013.DeliveryService.Package();
            dto.Address = new schema.skspackage.org._2013.DeliveryService.Address();
            dto.Address.City = entity.Address.Location;
            dto.Address.PostalCode = entity.Address.Postcode;
            dto.Address.Street = entity.Address.Street;
            dto.Address.Country = "Austria";

            return dto;
        }
    }
}

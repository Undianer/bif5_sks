﻿namespace ShippingService.Mapper
{
    public class PackageMapper
    {
        public EntityRepo.Entities.Package MapDtoToPackage(Package dto)
        {
            EntityRepo.Entities.Package package = new EntityRepo.Entities.Package
            (
                "Anonymous",
                dto.Address.City,
                dto.Address.PostalCode,
                dto.Address.Street
            );

            return package;
        }

    }
}
﻿using DAL.OutMemory;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using log4net;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ShippingService.Mapper;

namespace ShippingService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
	public class ShippingService : IShippingService
	{
		IPackageShipper PackageShipper { get; set; }
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		public void AddPackage(Package package)
		{
			var container = new UnityContainer().LoadConfiguration();
			PackageShipper = container.Resolve<IPackageShipper>();
			EntityRepo.Entities.Package result = new PackageMapper().MapDtoToPackage(package);
            try
            {
                PackageShipper.AddPackage(result);
            }			
            catch(RepositoryException ex)
            {
                log.Error("Error occured in ShippingService.PackageShipper.AddPackage", ex);
            }
		}

		public System.Threading.Tasks.Task AddPackageAsync(Package package)
		{
			throw new NotImplementedException();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

/*
 * visual studio 2012 command prompt
 * ind dne sks folder gehen
 * xsd /c XmlRegionImportSchema.xsd
 */

namespace RestService
{
	// Windows Communication Foundation
	// HINWEIS: Mit dem Befehl "Umbenennen" im Menü "Umgestalten" können Sie den Schnittstellennamen "IService1" sowohl im Code als auch in der Konfigurationsdatei ändern.
	[ServiceContract]
	public interface IRegionUploadService
	{

		[OperationContract]
		[WebGet(ResponseFormat = WebMessageFormat.Xml, UriTemplate = "/Regions/{name}")]
		RegionData GetRegion(string name);

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Xml, UriTemplate = "/Import/Region")]
		[XmlSerializerFormat]
		void ImportRegion(RegionData r);

		//[OperationContract]
		//CompositeType GetDataUsingDataContract(CompositeType composite);
		// Fiddler

	}


	// Verwenden Sie einen Datenvertrag, wie im folgenden Beispiel dargestellt, um Dienstvorgängen zusammengesetzte Typen hinzuzufügen.
	[DataContract]
	public class CompositeType
	{
		bool boolValue = true;
		string stringValue = "Hello ";

		[DataMember]
		public bool BoolValue
		{
			get { return boolValue; }
			set { boolValue = value; }
		}

		[DataMember]
		public string StringValue
		{
			get { return stringValue; }
			set { stringValue = value; }
		}
	}
}

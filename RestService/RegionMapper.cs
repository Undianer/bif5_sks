﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityRepo.Entities;

namespace RestService
{
	public class RegionMapper
	{
		public List<Region> MapRegionDataToRegion(RegionData r)
		{
			List<Region> result = new List<Region>();
			foreach (RegionDataRegion xmlRegion in r.Region)
			{
				
				Region region = new Region(
                        xmlRegion.Key,
						xmlRegion.DisplayName,
						xmlRegion.Address.City,
						xmlRegion.Address.PostalCode,
						xmlRegion.Address.Street
					);

				result.Add(region);
			}

			return result;
		}

		public RegionData MapRegionListToRegionData(List<Region> regionList)
		{
			RegionData r = new RegionData();
			r.Region = new RegionDataRegion[regionList.Count];

			for (int i = 0; i < regionList.Count; i++)
			{
				RegionDataRegion rdr = new RegionDataRegion()
				{
					Key = regionList[i].Key,
					DisplayName = regionList[i].Name,
					Address = new RegionDataRegionAddress()
					{
						City = regionList[i].Address.Location,
						PostalCode = regionList[i].Address.Postcode,
						Street = regionList[i].Address.Street
					}
				};
                
				r.Region[i] = rdr;
			}

			return r;
		}
	}
}
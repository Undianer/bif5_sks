﻿using DAL.OutMemory;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RestService
{
	// HINWEIS: Mit dem Befehl "Umbenennen" im Menü "Umgestalten" können Sie den Klassennamen "Service1" sowohl im Code als auch in der SVC- und der Konfigurationsdatei ändern.
	// HINWEIS: Wählen Sie zum Starten des WCF-Testclients zum Testen dieses Diensts Service1.svc oder Service1.svc.cs im Projektmappen-Explorer aus, und starten Sie das Debuggen.
	public class RegionUploadService : IRegionUploadService
	{
		private IRegionImporter RegionImporter { get; set; }
		private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(RegionUploadService));

		#region
		public string GetData(string value)
		{
			return string.Format("You entered: {0}", value);
		}

		public CompositeType GetDataUsingDataContract(CompositeType composite)
		{
			if (composite == null)
			{
				throw new ArgumentNullException("composite");
			}
			if (composite.BoolValue)
			{
				composite.StringValue += "Suffix";
			}
			return composite;
		}
		#endregion

		public RegionData GetRegion(string name)
		{
			var container = new UnityContainer().LoadConfiguration();

			RegionImporter = container.Resolve<IRegionImporter>();

			log.Info("RegionUploadService.GetRegion start");
			List<Region> regionList = new List<Region>(RegionImporter.GetRegion(name));
			RegionMapper mapper = new RegionMapper();

			return mapper.MapRegionListToRegionData(regionList);
		}

		public void ImportRegion(RegionData r)
		{
			log.Info("RegionUploadService.ImportRegion start");

			var container = new UnityContainer().LoadConfiguration();
			//container.RegisterType<IRegionImporter, RegionImporter>()
			//	.RegisterType<IRegionRepo, DbRegionRepo>()
			//	.RegisterType<IGeodataServiceAgent, GeodataServiceAgent>();

			RegionImporter = container.Resolve<IRegionImporter>();
			IGeodataServiceAgent geodataService = container.Resolve<IGeodataServiceAgent>();
			RegionMapper mapper = new RegionMapper();

			try
			{
				bool result = RegionImporter.ImportRegion(mapper.MapRegionDataToRegion(r), geodataService);
			}
			catch (RepositoryException ex)
			{
				log.Error("in WCF Import/Region", ex);
			}
		}
	}
}

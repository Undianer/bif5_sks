﻿using EntityRepo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegrationTestRestService.PackageDeliveryService
{
    /// <summary>
    /// Summary description for PackageDeliveryService_Test
    /// </summary>
    [TestClass]
    public class PackageDeliveryService_Test
    {
        public PackageDeliveryService_Test()
        {
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
		private void DeleteDb()
		{
			using (var db = new EntityContext())
			{
				db.Database.ExecuteSqlCommand("delete from dbo.Packages");
				db.Database.ExecuteSqlCommand("delete from dbo.Regions");
				db.Database.ExecuteSqlCommand("delete from dbo.Addresses");
				db.Database.ExecuteSqlCommand("delete from dbo.Coords");
			}
		}

		//Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
		[TestInitialize()]
		public void MyTestInitialize() { DeleteDb(); }

		//Mit TestCleanup können Sie nach jedem einzelnen Test Code ausführen.
		[TestCleanup()]
		public void MyTestCleanup() { DeleteDb(); }
        #endregion

        [TestMethod, TestCategory("IntegrationTest")]
        public void PackageDeliveryService_GetPackages_Integration()
        {
            //Arrange
            Package[] result;
			EntityRepo.Entities.Package toAdd = new EntityRepo.Entities.Package("Stefan Opitz", "Wien", "1200", "Hochstädtplatz 4");
			EntityRepo.Entities.Package toAdd2 = new EntityRepo.Entities.Package("Andreas Unterbrunner", "Wien", "1200", "Hochstädtplatz 4");
	        EntityRepo.Entities.Region region = new EntityRepo.Entities.Region("W1200", "Wien 20", toAdd.Address.Location,
		        toAdd.Address.Postcode, toAdd.Address.Street);
	        toAdd.Region = region;
	        toAdd2.Region = region;
            IPackageDeliveryService deliveryService = new PackageDeliveryServiceClient();
	        using (var context = new EntityContext())
	        {
		        context.Regions.Add(region);
		        context.Packages.Add(toAdd);
		        context.Packages.Add(toAdd2);
		        context.SaveChanges();
	        }
    
            //Act
            result = deliveryService.GetPackagesForRegion(region.Key);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);
        }
    }
}

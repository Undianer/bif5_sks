﻿using System.Data.Entity;
using System.Linq;
using EntityRepo;
using EntityRepo.Entities;
using IntegrationTestRestService.ShippingService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Address = IntegrationTestRestService.ShippingService.Address;
using Package = IntegrationTestRestService.ShippingService.Package;

namespace IntegrationTestRestService.PackageShippingService
{
    /// <summary>
    /// Summary description for ShippingService_Test
    /// </summary>
    [TestClass]
    public class ShippingService_Test
    {
        Package Dto { get; set; }

        public ShippingService_Test()
        {
            Dto = new Package();
            Dto.Address = new Address();
            Dto.Address.City = "Vienna";
            Dto.Address.Country = "Austria";
            Dto.Address.PostalCode = "1200";
            Dto.Address.Street = "Höchststädtplatz 4";
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        private void DeleteDb()
        {
            using (var db = new EntityContext())
            {
				db.Database.ExecuteSqlCommand("delete from dbo.Packages");
				db.Database.ExecuteSqlCommand("delete from dbo.Regions");
				db.Database.ExecuteSqlCommand("delete from dbo.Addresses");
				db.Database.ExecuteSqlCommand("delete from dbo.Coords");
            }
        }
        //Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
        [TestInitialize()]
        public void MyTestInitialize() { DeleteDb(); }

        //Mit TestCleanup können Sie nach jedem einzelnen Test Code ausführen.
        [TestCleanup()]
        public void MyTestCleanup() { DeleteDb(); }
        //
        #endregion

        [TestMethod, TestCategory("IntegrationTest")]
        public void ShippingService_AddPackage_Integration()
        {
            // Arrange
            IShippingService shippingService = new ShippingServiceClient();
            EntityRepo.Entities.Package result;
			IQueryable<EntityRepo.Entities.Package> query;
			EntityRepo.Entities.Region region = new Region("W1200", "Wien 20", "Vienna", "1200", "Höchstädtplatz 5");
	        using (var db = new EntityContext())
	        {
		        db.Regions.Add(region);
		        db.SaveChanges();
	        }

            // Act
            shippingService.AddPackage(Dto);

            // Assert
            using( var db = new EntityRepo.EntityContext() )
            {
				query = db.Packages.AsQueryable();
				result = query.Include(p => p.Address).AsEnumerable().LastOrDefault();
            }

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Address.Location, Dto.Address.City);
        }
    }
}

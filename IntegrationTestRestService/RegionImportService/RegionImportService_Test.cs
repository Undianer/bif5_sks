﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Serialization;
using EntityRepo;
using EntityRepo.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegionData = RestService.RegionData;
using RegionDataRegion = RestService.RegionDataRegion;
using RegionDataRegionAddress = RestService.RegionDataRegionAddress;

namespace IntegrationTestRestService.RegionImportService
{
	/// <summary>
	/// Zusammenfassungsbeschreibung für RegionImportService_Test
	/// </summary>
	[TestClass]
	public class RegionImportService_Test
	{
		public RegionData Data { get; set; }

		public RegionImportService_Test()
		{
			Data = new RegionData
			{
				Region = new[]
                {
                    new RegionDataRegion()
                    {
                        Key = "W1170",
                        DisplayName = "Wien_1170",
                        Address = new RegionDataRegionAddress()
                        {
                            City = "Wien",
                            PostalCode = "1170",
                            Street = "Wattgasse 56-60"
                        }
                    },
                    new RegionDataRegion()
                    {
                        Key = "W1200",
                        DisplayName = "Wien_1200",
                        Address = new RegionDataRegionAddress()
                        {
                            City = "Wien",
                            PostalCode = "1200",
                            Street = "Dresdner Straße 116-118"
                        }
                    },
                    new RegionDataRegion()
                    {
                        Key = "W1100",
                        DisplayName = "Wien_1100",
                        Address = new RegionDataRegionAddress()
                        {
                            City = "Wien",
                            PostalCode = "1100",
                            Street = "Buchengasse 77"
                        }
                    }
                }
			};
		}

		//private TestContext testContextInstance;

		#region Zusätzliche Testattribute

		private void DeleteDb()
		{
			using (var db = new EntityContext())
			{
				db.Database.ExecuteSqlCommand("delete from dbo.Packages");
				db.Database.ExecuteSqlCommand("delete from dbo.Regions");
				db.Database.ExecuteSqlCommand("delete from dbo.Addresses");
				db.Database.ExecuteSqlCommand("delete from dbo.Coords");
			}
		}

		//Mit TestInitialize können Sie vor jedem einzelnen Test Code ausführen. 
		[TestInitialize()]
		public void MyTestInitialize() { DeleteDb(); }

		//Mit TestCleanup können Sie nach jedem einzelnen Test Code ausführen.
		[TestCleanup()]
		public void MyTestCleanup() { DeleteDb(); }

		#endregion

		[TestMethod, TestCategory("IntegrationTest")]
		public void ImportRegion_RegionImportService_FromRestService()
		{
			// Arrange
			Uri requestUri = new Uri("http://localhost:7979/RegionUploadService.svc/Import/Region");
			List<Region> result = null;

			// Act
			try
			{
				byte[] byteData;
				using (var stream = new MemoryStream())
				{
					var xs = new XmlSerializer(typeof(RegionData));
					xs.Serialize(stream, Data);
					byteData = stream.ToArray();
				}

				var client = new WebClient();
				client.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
				client.UploadData(requestUri, "POST", byteData);
			}
			catch (WebException webProblem)
			{
				Console.WriteLine(webProblem.Message);
			}

			// Assert
			using (var context = new EntityContext())
			{
				result = context.Regions.ToList();
			}

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count >= Data.Region.Length);
		}
	}
}

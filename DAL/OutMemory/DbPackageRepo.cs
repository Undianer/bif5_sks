﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo;
using EntityRepo.Entities;
using System.Data.Entity;

namespace DAL.OutMemory
{
	public class DbPackageRepo : IPackageRepo
	{
        private readonly log4net.ILog log;

        public DbPackageRepo()
        {
            log = log4net.LogManager.GetLogger(typeof(DbPackageRepo));
        }

		public IEnumerable<Package> GetByLocation(string location)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Address)
						.Where(p => p.Address.Location == location)
						.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.GetByLocation Fehler");
				throw new RepositoryException("in DbPackageRepo.GetByLocation ", ex);
			}
		}

		public IEnumerable<Package> GetByPostcode(string postcode)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Address)
						.Where(p => p.Address.Postcode == postcode)
						.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.GetByPostcode Fehler");
				throw new RepositoryException("in DbPackageRepo.GetByPostcode ", ex);
			}
		}

		public IEnumerable<Package> GetByStreet(string location, string street)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Address)
						.Where(p => p.Address.Location == location).Where(p => p.Address.Street == street)
						.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.GetByStreet with location Fehler", ex);
				throw new RepositoryException("in DbPackageRepo.GetByStreet(location, street) ", ex);
			}
		}

		public IEnumerable<Package> GetByStreetAndPostcode(string postcode, string street)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Address)
						.Where(p => p.Address.Postcode == postcode).Where(p => p.Address.Street == street)
						.ToList();
				}

			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.GetByStreetAndPostcode with postcode Fehler", ex);
				throw new RepositoryException("in DbPackageRepo.GetByStreet(postcode, street) ", ex);
			}
		}

		public IEnumerable<Package> GetByAddress(Address address)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Address)
						.Where(p => p.Address.Id == address.Id)
						.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.GetByAddress Fehler");
				throw new RepositoryException("in DbPackageRepo.GetByAddress ", ex);
			}
		}

		public void Add(Package obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					Region region;
					region = db.Regions.FirstOrDefault(r => r.Id == obj.Region.Id);

					if (region == null)
					{
						region = db.Regions
							.FirstOrDefault(r => r.Key.Equals(obj.Region.Key) && obj.Region.Name.Equals(r.Name));

						if (region != null)
						{
							obj.Region = region;
						}
					}
					else
					{
						obj.Region = region;
					}

					db.Packages.Add(obj);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.Add Fehler");
				throw new RepositoryException("in DbPackageRepo.Add ", ex);
			}
		}

		public void Update(Package old, Package obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					db.Packages.Attach(obj);
					db.Entry(obj).State = EntityState.Modified;
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.Update Fehler");
				throw new RepositoryException("in DbPackageRepo.Update ", ex);
			}
		}

		public void Delete(Package obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					var query = (from x in db.Packages where x.Id == obj.Id select x).First();
					db.Packages.Remove(query);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.Delete Fehler");
				throw new RepositoryException("in DbPackageRepo.Delete ", ex);
			}
		}

		public IEnumerable<Package> GetAll()
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Region)
						.Include(p => p.Address)
						.Include(p => p.Coords)
						.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbPackageRepo.GetAll Fehler", ex);
				throw new RepositoryException("in DbPackageRepo.GetAll ", ex);
			}
		}

		public IEnumerable<Package> GetByRegionKey(string key)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Region)
						.Where(p => p.Region.Key.Equals(key))
						.ToList();
				}
			}
			catch (Exception ex)
			{
				log.Error("DbPackageRepo.GetByRegionKey Fehler", ex);
				throw new RepositoryException("in DbPackageRepo.GetByRegionKey ", ex);
			}
		}

		public IEnumerable<Package> DeliverPackagesFromRegion(string key)
		{
			try
			{
				using (var db = new EntityContext())
				{
					var query = db.Packages
						.Include(p => p.Region)
						.Include(p => p.Address)
						.Include(p => p.Coords)
						.Where(p => p.Region.Key.Equals(key) && p.Delivered == false)
						.AsQueryable();

					foreach (var package in query)
					{
						package.Delivered = true;
						db.Entry(package).State = EntityState.Modified;
					}
					//list.ForEach(p =>
					//{
					//	p.Delivered = true;
					//	db.Entry(p).State = EntityState.Modified;
					//});

					var list = query.ToList();
					db.SaveChanges();
					return list;
				}
			}
			catch (Exception ex)
			{
				log.Error("DbPackageRepo.DeliverPackagesFromRegion Fehler", ex);
				throw new RepositoryException("in DbPackageRepo.DeliverPackagesFromRegion ", ex);
			}
		}


		public IEnumerable<Package> GetDeliveredPackagesByRegionKey(string key, bool state)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Packages
						.Include(p => p.Region)
						.Where(p => p.Region.Key.Equals(key) && p.Delivered == state)
						.ToList();
				}
			}
			catch (Exception ex)
			{
				log.Error("DbPackageRepo.GetDeliveredPackagesByRegionKey Fehler", ex);
				throw new RepositoryException("in DbPackageRepo.GetDeliveredPackagesByRegionKey ", ex);
			}
		}
	}
}

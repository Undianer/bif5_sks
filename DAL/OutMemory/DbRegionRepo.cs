﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo;
using EntityRepo.Entities;
using System.Data.Entity;

namespace DAL.OutMemory
{
	public class DbRegionRepo : IRegionRepo
	{
        private readonly log4net.ILog log;

        public DbRegionRepo() 
        {
            log = log4net.LogManager.GetLogger(typeof(DbRegionRepo));
        }

		public void Add(Region obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					db.Regions.Add(obj);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbRegionRepo.Add Fehler", ex);
				throw new RepositoryException("in DbRegionRepo.Add ", ex);
			}
		}

		public void Update(Region old, Region obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					db.Regions.Attach(obj);
					db.Entry(obj).State = EntityState.Modified;
					db.Addresses.Attach(obj.Address);
					db.Entry(obj.Address).State = EntityState.Modified;
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbRegionRepo.Update Fehler", ex);
				throw new RepositoryException("in DbRegionRepo.Update ", ex);
			}

		}

		public void Delete(Region obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					var queryRegion = (from x in db.Regions where x.Id == obj.Id select x).First();
					var queryPackage = (from y in db.Packages where y.Region.Id == obj.Id select y).ToList();

					foreach (Package p in queryPackage)
					{
						db.Packages.Find(p.Id).Region = null;
					}

					db.Regions.Remove(queryRegion);
					db.SaveChanges();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbRegionRepo.Delete Fehler", ex);
				throw new RepositoryException("in DbRegionRepo.Delete ", ex);
			}
		}

		public IEnumerable<Region> GetAll()
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Regions
						.Include(r => r.Address)
						.Include(r => r.Coords)
						.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbRegionRepo.GetAll Fehler", ex);
				throw new RepositoryException("in DbRegionRepo.GetAll ", ex);
			}
		}

		public IEnumerable<Region> GetByName(string name)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Regions.Where(r => r.Name == name).ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbRegionRepo.GetByName Fehler", ex);
				throw new RepositoryException("in DbRegionRepo.GetByName ", ex);
			}
		}
        public IEnumerable<Region> GetByKey(string key)
        {
            try
            {
                using (var db = new EntityContext())
                {
                    return db.Regions.Where(r => r.Key == key).ToList();
                }
            }
            catch (Exception ex)
            {
                log.Error("DbRegionRepo.GetByKey Fehler", ex);
                throw new RepositoryException("in DbRegionRepo.GetByKey ", ex);
            }
        }

		public Region GetById(int id)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.Regions
						.Include(r => r.Address)
						.Include(r => r.Coords)
						.FirstOrDefault(r => r.Id == id);
				}
			}
			catch (Exception ex)
			{
				log.Error("DbRegionRepo.GetById Fehler", ex);
				throw new RepositoryException("in DbRegionRepo.GetById ", ex);
			}
		}
	}
}

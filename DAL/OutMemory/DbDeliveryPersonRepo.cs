﻿

using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo;
using EntityRepo.Entities;
using System.Data.Entity;

namespace DAL.OutMemory
{
	public class DbDeliveryPersonRepo : IDeliveryPersonRepo
	{
        private readonly log4net.ILog log;

       public  DbDeliveryPersonRepo()
        {
           log = log4net.LogManager.GetLogger(typeof(DbDeliveryPersonRepo));
        }

		public IEnumerable<DeliveryPerson> GetByName(string name)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.DeliveryPersons.Where(dp => dp.Name == name);
				}
			}
			catch (Exception ex)
			{
                log.Error("DbDeliverPersonRepo.GetByName Fehler");
				throw new RepositoryException("in DbDerliveryPersonRepo.GetByName ", ex);
			}
		}

		public DeliveryPerson GetBySvn(int svn)
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.DeliveryPersons.Where(dp => dp.Svn == svn).Single(); //Single weil Rückgabe nicht die Liste
				}
			}
			catch (Exception ex)
			{
                log.Error("DbDeliverPersonRepo.GetBySvn Fehler");
				throw new RepositoryException("in DbDerliveryPersonRepo.GetBySvn ", ex);
			}
		}

		public void Add(DeliveryPerson obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					db.DeliveryPersons.Attach(obj);
					db.Entry(obj).State = EntityState.Added;
					db.SaveChanges();
				}
			}
			catch (Exception ex)
            {
                log.Error("DbDeliverPersonRepo.Add Fehler");
				throw new RepositoryException("in DbDerliveryPersonRepo.Add ", ex);
			}
		}

		public void Update(DeliveryPerson old, DeliveryPerson obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					db.DeliveryPersons.Attach(obj);
					db.Entry(obj).State = EntityState.Modified;
					db.SaveChanges();
				}
			}
			catch (Exception ex)
            {
                log.Error("DbDeliverPersonRepo.Update Fehler");
				throw new RepositoryException("in DbDerliveryPersonRepo.Update ", ex);
			}
		}

		public void Delete(DeliveryPerson obj)
		{
			try
			{
				using (var db = new EntityContext())
				{
					db.DeliveryPersons.Attach(obj);
					db.Entry(obj).State = EntityState.Deleted;
					db.SaveChanges();
				}
			}
			catch (Exception ex)
            {
                log.Error("DbDeliverPersonRepo.Delete Fehler");
				throw new RepositoryException("in DbDerliveryPersonRepo.Delete ", ex);
			}
		}

		public IEnumerable<DeliveryPerson> GetAll()
		{
			try
			{
				using (var db = new EntityContext())
				{
					return db.DeliveryPersons.ToList();
				}
			}
			catch (Exception ex)
			{
                log.Error("DbDeliverPersonRepo.GetAll Fehler");
				throw new RepositoryException("in DbDerliveryPersonRepo.GetAll ", ex);
			}
		}
	}
}

﻿using System.Collections.Generic;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo.Entities;

namespace DAL.Mock
{
	public class InMemoryDeliveryPerson : IDeliveryPersonRepo
	{
		private List<DeliveryPerson> delPerList;

		public InMemoryDeliveryPerson()
		{
			delPerList = new List<DeliveryPerson>();
			// Konstruktor
		}

		public List<DeliveryPerson> DeliveryPersonList
		{
			get { return delPerList; }
			set { delPerList = value; }
		}

		public IEnumerable<DeliveryPerson> GetByName(string name)
		{
			List<DeliveryPerson> result = new List<DeliveryPerson>();
			foreach (DeliveryPerson delPerson in delPerList)
			{
				if (delPerson.Name.Equals(name))
					result.Add(delPerson);
			}

			return result;
		}

		public DeliveryPerson GetBySvn(int svn)
		{
            DeliveryPerson result = null;
            foreach (DeliveryPerson delPerson in delPerList)
            {
                if (delPerson.Svn == svn)
                    result = delPerson;
            }
            
            return result;
		}

		public void Add(DeliveryPerson obj)
		{
            delPerList.Add(obj);
		}

		public void Update(DeliveryPerson old, DeliveryPerson obj)
		{
            int i = delPerList.IndexOf(old);
            delPerList.RemoveAt(i);
            delPerList.Insert(i, obj);
		}

		public void Delete(DeliveryPerson obj)
		{
            delPerList.Remove(obj);
		}

		public IEnumerable<DeliveryPerson> GetAll()
		{
            return delPerList;
		}
	}
}

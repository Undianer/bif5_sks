﻿using System.Collections.Generic;
using System.Linq;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo.Entities;

namespace DAL.Mock
{
	public class InMemoryPackage : IPackageRepo
	{
        public List<Package> PackageList { get; set; }
	
        public InMemoryPackage()
		{
			this.PackageList = new List<Package>();
 			this.PackageList.Add(new Package("Dawn Alolino", "Wien", "1200", "Dresdner Strasse 3"));
			this.PackageList.Add(new Package("Agnes Unterbrunner", "Wien", "1200", "Laengenfeldgasse 95"));
			this.PackageList.Add(new Package("Michael Opitz", "Gallbrunn", "2463", "Dresdner Strasse 3"));
			this.PackageList.ForEach(p => p.Region = new Region("W1200", "Wien 20er", "Wien", "1200", "Dresdner Strasse 3"));
		}
	
		public IEnumerable<Package> GetByLocation(string location)
		{
			List<Package> result = new List<Package>();
			foreach( Package item in PackageList) 
			{
				if (item.Address.Location.Equals(location))
					result.Add(item);
			}

			return result;
		}

		public IEnumerable<Package> GetByPostcode(string postcode)
		{
			List<Package> result = new List<Package>();
			foreach (Package item in PackageList)
			{
				if (item.Address.Postcode.Equals(postcode))
					result.Add(item);
			}

			return result;
		}

		public IEnumerable<Package> GetByStreet(string location, string street)
		{
			List<Package> result = new List<Package>();
			foreach (Package item in PackageList)
			{
				if ( item.Address.Location.Equals(location) && item.Address.Street.Equals(street) )
					result.Add(item);
			}

			return result;
		}

		public IEnumerable<Package> GetByStreetAndPostcode(string postcode, string street)
		{
			List<Package> result = new List<Package>();
			foreach (Package item in PackageList)
			{
				if (item.Address.Postcode.Equals(postcode) && item.Address.Street.Equals(street))
					result.Add(item);
			}

			return result;
		}

		public IEnumerable<Package> GetByAddress(Address address)
		{
			List<Package> result = new List<Package>();
			foreach (Package item in PackageList)
			{
				if (item.Address.Equals(address))
					result.Add(item);
			}

			return result;
		}

		public void Add(Package obj)
		{
			this.PackageList.Add(obj);
		}

		public void Update(Package old, Package obj)
		{
			int index = PackageList.IndexOf(old);
			PackageList.RemoveAt(index);
			PackageList.Insert(index, obj);

		}

		public void Delete(Package obj)
		{
			PackageList.Remove(obj);
		}

		public IEnumerable<Package> GetAll()
		{
			return this.PackageList;
		}


		public IEnumerable<Package> GetByRegionKey(string key)
		{
			return this.PackageList.Where(p => p.Region.Key.Equals(key)).AsEnumerable();
		}

		public IEnumerable<Package> DeliverPackagesFromRegion(string key)
		{
			var query = this.PackageList.Where(p => p.Region.Key.Equals(key) && p.Delivered == false).ToList();
			query.ForEach(p => p.Delivered = true);
			return query;
		}


		public IEnumerable<Package> GetDeliveredPackagesByRegionKey(string key, bool state)
		{
			return this.PackageList.Where(p => p.Region.Key.Equals(key) && p.Delivered == state);
		}
	}
}

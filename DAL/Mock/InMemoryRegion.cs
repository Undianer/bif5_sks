﻿using System.Collections.Generic;
using DALInterfaces.Interfaces.Repositories;
using EntityRepo.Entities;

namespace DAL.Mock
{
    public class InMemoryRegion : IRegionRepo
    {
        private List<Region> regList;

        public InMemoryRegion()
        {
        }

        public List<Region> RegList
        {
            get { return regList; }
            set { regList = value; }
        }

        public void Add(Region obj)
        {
            regList.Add(obj);
        }

        public void Update(Region old, Region obj)
        {
            int i = regList.IndexOf(old);
            regList.RemoveAt(i);
            regList.Insert(i, obj);
        }

        public void Delete(Region obj)
        {
            regList.Remove(obj);
        }

        public IEnumerable<Region> GetAll()
        {
            return regList;
        }

        public IEnumerable<Region> GetByName(string name)
        {
            List<Region> result = new List<Region>();
            foreach (Region reg in regList)
            {
                if (reg.Name.Equals(name))
                    result.Add(reg);
            }

            return result;
        }
        public IEnumerable<Region> GetByKey(string key)
        {
            List<Region> result = new List<Region>();
            foreach (Region reg in regList)
            {
                if (reg.Key.Equals(key))
                    result.Add(reg);
            }

            return result;
        }


		public Region GetById(int id)
		{
			return regList.Find(r => r.Id == id);
		}
	}
}

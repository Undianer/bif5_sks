﻿using System;
using DAL.Mock;
using DAL.OutMemory;
using DALInterfaces.Interfaces.Repositories;

namespace DAL
{
    public class RepositoryFactory
    {
		public static string Db = "db";
		public static string Mock = "mock";

        public static IDeliveryPersonRepo CreateDeliveryPersonRepo(string type)
        {
            if (type == "db")
            {
                return new DbDeliveryPersonRepo();
            }
            else if (type == "mock")
            {
                return new InMemoryDeliveryPerson();
            }
            throw new ArgumentException("Invalid Type passed into Factory");
        }

        public static IPackageRepo CreatePackageRepo(string type)
        {
            if (type == "db")
            {
                return new DbPackageRepo();
            }
            else if (type == "mock")
            {
                return new InMemoryPackage();
            }
            throw new ArgumentException("Invalid Type passed into Factory");
        }

        public static IRegionRepo CreateRegionRepo(string type)
        {
            if (type == "db")
            {
                return new DbRegionRepo();
            }
            else if (type == "mock")
            {
                return new InMemoryRegion();
            }
            throw new ArgumentException("Invalid Type passed into Factory");
        }
    }
}

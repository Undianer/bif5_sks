﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityRepo.Entities;

namespace opun.sks.Presentation.Website.Models
{
	public class RegionsPackagesViewModel
	{
		public List<Package> Packages { get; set; }
		public List<Region> Regions { get; set; }

		public RegionsPackagesViewModel()
		{
			Packages = new List<Package>();
			Regions = new List<Region>();
		}
	}
}
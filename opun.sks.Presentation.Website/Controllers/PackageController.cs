﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DAL.OutMemory;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Presentation;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using GeodataService;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Opitz_Unterbrunner_SKS;
using Opitz_Unterbrunner_SKS.PresentationLogic;
using opun.sks.Presentation.Website.Models;

namespace opun.sks.Presentation.Website.Controllers
{
	public class PackageController : Controller
	{
		private IRegionLogic regionLogic;
		private IPackageLogic packageLogic;
		private List<Package> packages;
		public List<SelectListItem> itemList { get; set; }
		IUnityContainer container { get; set; }

		public PackageController()
		{
			container = new UnityContainer();
			
			container.LoadConfiguration();

			regionLogic = container.Resolve<IRegionLogic>();
			packageLogic = container.Resolve<IPackageLogic>();

			List<Region> regions = new List<Region>(regionLogic.GetAll());
			var items = regions.Select(r => new SelectListItem
					{
						Value = r.Key,
						Text = r.Key
					});
			itemList = new List<SelectListItem>(items);
		}

		//
		// GET: /Package/
		public ActionResult Index()
		{
			packages = new List<Package>(packageLogic.GetAll());
			packages = packages.Where(p => p.Delivered == false).ToList();
			ViewBag.RegionKeys = itemList;

			return View(packages);
		}

		[HttpPost]
		public ActionResult Index(string selectRegion)
		{
			ViewBag.SelectedKey = selectRegion;
			packages = new List<Package>(packageLogic.GetDeliveredByRegionKey(selectRegion, false));
			ViewBag.RegionKeys = itemList;

			return View(packages);
		}

		[HttpPost]
		public ActionResult Delivered(FormCollection collection)
		{
			string key = collection["deliver"];
			packages = new List<Package>(packageLogic.DeliverPackagesFromRegion(key));

			return View(packages);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace opun.sks.Presentation.Website.Controllers
{
	public class HomeController : Controller
	{

		public ActionResult Index()
		{
            log4net.Config.XmlConfigurator.Configure();
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "BIF-5 Software Komponenten Systeme";

			return View();
		}

		public ActionResult Contact()
		{
			return View();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.OutMemory;
using DALInterfaces.Exceptions;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Presentation;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using GeodataService;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Opitz_Unterbrunner_SKS;
using Opitz_Unterbrunner_SKS.PresentationLogic;
using log4net;

namespace opun.sks.Presentation.Website.Controllers
{
    public class RegionController : Controller
    {
	    private IRegionLogic regionLogic;
		IUnityContainer container { get; set; }
        private readonly log4net.ILog log;
	    public RegionController()
	    {
			container = new UnityContainer();
            log = log4net.LogManager.GetLogger(typeof(RegionController));
		    container.LoadConfiguration();

		    regionLogic = container.Resolve<IRegionLogic>();
	    }

        //
        // GET: /Region/
        public ActionResult Index()
        {
	        List<Region> regions = null;
	        try
	        {
                log.Info("Regions werden geladen");
		        regions = new List<Region>(regionLogic.GetAll());
	        }
	        catch (RepositoryException ex)
	        {
	
                log.Error("Website.RegionController.Index(), Fehler beim Übergeben der Regions an den View", ex);
		        regions = new List<Region>();
	        }
	        return View(regions);
        }

        //
        // GET: /Region/Edit/5
		public ActionResult Edit(int id)
		{
			Region region = null;
			try
			{
				region = regionLogic.GetById(id);
			}
			catch (RepositoryException ex)
			{
                log.Error("Website.RegionController.Edit(int id), Fehler beim Übergeben der Region an den View", ex);
			}

			return View(region);
		}

        //
        // POST: /Region/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
	            Region region = regionLogic.GetById(id);
	            region.Name = collection["name"];
	            region.Address.Location = collection["location"];
	            region.Address.Postcode = collection["postcode"];
				region.Address.Street = collection["street"];

				regionLogic.Update(region);

                return RedirectToAction("Index");
            }
            catch (RepositoryException ex)
            {
                log.Error("Website.RegionController.Edit(int id, FormCollection collection), Fehler beim Update der Region", ex);

                return View();
            }
        }

        //
        // GET: /Region/Delete/5
        public ActionResult Delete(int id)
        {
	        Region region = null;
	        try
	        {
		        region = regionLogic.GetById(id);
	        }
	        catch (RepositoryException ex)
	        {
                log.Error("Website.RegionController.Delete(int id), Fehler beim Laden der Region", ex);

		        region = null;
	        }

	        return View(region);
        }

        //
        // POST: /Region/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
        
	            Region delete = regionLogic.GetById(id);
				regionLogic.Delete(delete);

                return RedirectToAction("Index");
            }
            catch (RepositoryException ex)
            {
                log.Error("Website.RegionController.Delete(int id, FormCollection collection), Fehler beim Löschen der Region", ex);
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using DAL.OutMemory;
using DALInterfaces.Interfaces.Agents;
using DALInterfaces.Interfaces.Presentation;
using DALInterfaces.Interfaces.Repositories;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using GeodataService;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Opitz_Unterbrunner_SKS;
using Opitz_Unterbrunner_SKS.PresentationLogic;
using log4net;

namespace opun.sks.Presentation.Website.Controllers
{
    public class UploadController : Controller
    {
	    private IRegionLogic regionLogic;
	    public IUnityContainer Container { get; set; }
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

	    public UploadController()
	    {
		    Container = new UnityContainer();
			
		    Container.LoadConfiguration();

		    regionLogic = Container.Resolve<IRegionLogic>();
	    }

        //
        // GET: /Upload/
        public ActionResult Index()
        {
            return View();
        }

		//
		// GET: /Upload/Region/
	    public ActionResult Region()
	    {
		    return View();
	    }

		[HttpPost]
		// Post: /Upload/Region/
		public ActionResult Region(HttpPostedFileBase file)
		{
			if (file != null && ( file.ContentType.Equals("application/xml") || file.FileName.Contains(".xml") || file.ContentType.Equals("text/xml") ))
			{
                log.Info("Website.UploadController.Region(HttpPostedFileBase file), loading File");
				var reader = new StreamReader(file.InputStream);
				var doc = new XmlDocument();
				doc.Load(reader);
				
				if(regionLogic.ImportRegions(doc))
				return RedirectToAction("Index", "Region");
			}

			return View();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PackageDeliveryService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
	[System.ServiceModel.ServiceContractAttribute(Namespace = "http://sksPackage.org/2013/DeliveryService", ConfigurationName = "DeliveryService")]
	public interface IPackageDeliveryService
	{

		[System.ServiceModel.OperationContractAttribute(Action = "http://sksPackage.org/2013/DeliveryService/GetPackagesForRegion", ReplyAction = "http://sksPackage.org/2013/DeliveryService/GetPackagesForRegionResponse")]
		Package[] GetPackagesForRegion(string regionKey);

		//[System.ServiceModel.OperationContractAttribute(Action = "http://sksPackage.org/2013/DeliveryService/GetPackagesForRegion", ReplyAction = "http://sksPackage.org/2013/DeliveryService/GetPackagesForRegionResponse")]
		//System.Threading.Tasks.Task<Package[]> GetPackagesForRegionAsync(string regionKey);

	}
}

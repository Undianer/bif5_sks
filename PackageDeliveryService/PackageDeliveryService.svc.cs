﻿using System.Linq;
using DALInterfaces.Interfaces.Services;
using EntityRepo.Entities;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PackageDeliveryService.Mapper;

namespace PackageDeliveryService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
	public class PackageDeliveryService : IPackageDeliveryService
	{

		public Package[] GetPackagesForRegion(string regionKey)
		{
			var container = new UnityContainer().LoadConfiguration();

			IPackageDeliverer packageDeliverer = container.Resolve<IPackageDeliverer>();
			Package[] result;
			
			EntityRepo.Entities.Package[] packages = packageDeliverer.GetPackagesFromRegion(regionKey).ToArray();
            result = new PackageMapper().MapPackageArrayToDtoArray(packages);

			return result;
		}

		public System.Threading.Tasks.Task<Package[]> GetPackagesForRegionAsync(string regionKey)
		{
			throw new NotImplementedException();
		}

	}
}

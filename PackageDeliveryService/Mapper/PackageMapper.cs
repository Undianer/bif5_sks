﻿namespace PackageDeliveryService.Mapper
{
    public class PackageMapper
    {
        public Package MapPackageToDto(EntityRepo.Entities.Package entity)
        {
            Package dto = new Package();
            dto.Address = new Address();
            dto.Address.City = entity.Address.Location;
            dto.Address.PostalCode = entity.Address.Postcode;
            dto.Address.Street = entity.Address.Street;
            dto.Address.Country = "Austria";

            return dto;
        }

        public Package[] MapPackageArrayToDtoArray(EntityRepo.Entities.Package[] entityList)
        {
            Package[] dtoList = new Package[entityList.Length];
            for(int i=0; i < entityList.Length; i++)
            {
                dtoList[i] = MapPackageToDto(entityList[i]);
            }

            return dtoList;
        }
    }
}